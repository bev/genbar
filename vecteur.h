// version du 5 janvier 99
 
#if !defined(__VECTEUR_H)
#define __VECTEUR_H
#include <cmath>
#include <iostream>
#include <fstream>

#if defined PROJET
#include "config.h"
#else
#define TEMP "."
#define TABLEAU_TEST  1 // variable: TABLEAU_TEST =1 si on veut des tests sur les indices des tableaux 
#endif


#if defined ROOT
#include <TCanvas>  // pour graphisme
#endif

class Vecteur
{  public:

	// membres
	int i1,i2; //  indice du premier element et dernier
	int I;   // nombre d �l�ments
	double *element;

	//constructeurs
	Vecteur();
	Vecteur(int ii1, int ii2);  //met composantes a zero
       //	Vecteur(int ii2);
	Vecteur(const Vecteur & V);
 
	~Vecteur();


	// initialisateur-------------------------
	void Init(int ii1, int ii2);         // remplit de zero
	void Init(int ii1, int ii2,double a);

	//... remplit de valeurs al�atoires entre 0 et a
	void Aleatoire(double a=1);

	// libere memoire------------------------------
	void Libere();

	//operateurs--------------------------------------------------
	double & operator[](int i) const;       // selection d un element ex: V[3]
  	void operator=(double a);          // tous les elements mis � a.
	Vecteur & operator=(const Vecteur & v2); //recopie

	Vecteur & operator+=( const Vecteur & v2); // V+=W
	Vecteur & operator-=( const Vecteur & v2);  //V-=W

	Vecteur   operator+(const Vecteur & v2) const ;  //V=U+W
	Vecteur  operator-( const Vecteur & v2) const ; // V=U-W

	double operator*( const Vecteur & v2) const ;    // a=U*V

	friend Vecteur operator*(double a, const Vecteur &  v2);  // U=a*V
        friend Vecteur operator/(double a, const Vecteur &  v2);  // U=a/V  (a* inverse des elements)
	Vecteur operator*(double a) const ;  // U=V*a
	Vecteur operator/(double a) const ;  // U=V/a

	Vecteur operator+(double a) const ;  // U=V+a 
	Vecteur operator-(double a) const ;  // U=V-a 
	friend Vecteur operator+(double a, const Vecteur &  v2);  // U=a+V
	friend Vecteur operator-(double a, const Vecteur &  v2);   // U=a-V

	// operateurs unaires
	Vecteur operator+() const ;    // +U
	Vecteur operator-() const ;//  -U


void Remplit(double a);  // remplit le vecteur de  a


 double Norme();   // Calcule la norme
  double Norme2();   // Calcule la norme carr�
  void  Normer();   // Norme le vecteur

//====== renvoit la valeur absolue des elements ===============
Vecteur Abs();

/*========================================================
 recherche si la valeur val est dans le vecteur
 sortie: indice mini du vecteur  (-1001 si non trouve)
 */
int  Recherche(double val);

//============================================
// trouve le premier indice i tel que val< V[i+1]
// renvoit i   ( i2 si V[i2]<val )
int  Recherche_interval(double val);

/*========================================================
 renvoit indice  du premier max trouve
 */
int  Recherche_max();

/* trouve l'ordre croissant des elements
  sortie : vecteur : rang[i1,..,i2]   tel que rang[i1]= indice du premier element
*/
Vecteur  Ordre_croissant();

/* sortie : vecteur class�  */
 void  Range_croissant();

/*===================================
 Permutte les elements
  vecteur de permuttation: i2=rang[i1]; element i2 va en  i1.
  */
void Permutte(Vecteur & rang);
/*===================================
 Permutte circulaire de (d) elements
  */
void Permut_circ(int d);
/*=====================================
  Ajuste deux vecteurs V et W a valeur dans ]-Pi,Pi[,
 cad trouve la rotation qui minimise la somme des ecarts au carre.
 entree: V (this), W
 sortie: decalage d tel que,  V[i1 (premier element)]=W[i1+d] 

ex d utilisation:
    
       int d=V.Ajuste(W); // decalage
       cout<<"decalage="<<d<<endl;
       V.Permut_circ(d); // fait coincider V avec W
*/ 
int Ajuste(Vecteur  W);
/*=====================================================
 Cree un tableau * double
  et y copie le vecteur
	dans les places i=1,...,taille
						 j=1,...,taille
  ( � d�sallou� avec Tab1_del
*/
double * Tab1();
int Tab1_del(double * elem);
/*------------------------------------------
 Copie un tableau *double vers Vecteur
 */
int Tab1_copie(double * elem);

//------ pour afficher le contenu du vecteur
friend std::ostream & operator <<(std::ostream & sortie,  const Vecteur & vect);


/*---ecrit le contenu du vecteur dans un fichier --
format : I , i1, i2 , contenu
*/

void Ecrit_fichier(std::ofstream & fich);
void Ecrit_fichier(char const * nom_fich=TEMP"/vecteur.txt");
void Lit_fichier(std::ifstream & fich);
void Lit_fichier(char const * nom_fich=TEMP"/vecteur.txt");




#if defined ROOT
//========= dessin ========================
TCanvas*  Vecteur::Dessin(char * xtitre,char * ytitre,double xmin,double xmax,char * titre);
//========= dessin ========================
//  c : fenetre ou dessiner  
// superp: =0 si premier figure, 1,2.. si superposition
TCanvas*  Vecteur::Dessin(char * titre="Vecteur");
TCanvas*  Vecteur::Dessin(char * xtitre,char * ytitre,double xmin,double xmax,char * titre,TCanvas * c,int superp,char* opt="histo");

//========= dessin sur cercle ========================
void  Vecteur::Dessin_phases(char * titre);
//========= dessin sur colonne a l abcisse x========================
void  Vecteur::Dessin_col(double x, TCanvas *c);
//========= dessin (x[i],y[i] = objet)========================   
TCanvas*  Vecteur::Dessin(Vecteur &x,char * xtitre="x",char * ytitre="y",char * titre="Vecteur");
#endif

};
#endif
