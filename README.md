# genbar

An approach to identify genetic boundaries between populations using individual spatial co-ordinates and unlinked codominant markers.

# COMPILE

```
g++ genbar.epure.V1.3.cpp
```
