
//Je pense qu'on peut faire une computer note avec en entre coordonnees km et polaire, et donne microsat ou AFLP.


//*********************************************

//Programme �pur�- septembre 2005
// Programme modifi� avec m�thode de ranala au lieu de peatkau
// Donn�es entr�e en km ou en  degr�. Si km, sortie en km, si degre, sortie degr� (� v�rifierr)
// nombre d'alleles differents 500 au lieu de 5000 ==> il faudrait mettre cette valeur en variable : fait 
// nombre d'UG prev max dans UGt 2500 
//changement de la taille du tableau corres : 2500 
//ligne 172 : dans distance km changement UG en UGcoord
//Le recodage recode en -1 toutes les donn�es manquantes (valeurs initialement n�gatives)
//Modifications apportees pour donn�es manquantes dans proc recodage_donnees_pierres et calcul frequence_allelique
//HW ne marche pas avec donn�es manquantes:est-ce normal?? segmentation violation
//on enl�ve root: il faut alors modifier les classes de fr�d�rique
//Pour les fichiers ours, les coordonnees des UG sont en radian et la latitude est en pi/2-lat 
 

#include <iostream>
#include <fstream>
#include <strstream>
#include <vector>
#include "MersenneTwister.h"
#include "vecteur.h" //classe cree par frederic pour les tableaux
#include "matrice.h"  //classe cree par frederic pour les tableaux
#include "matrice.c"
#include "vecteur.c"
#include <algorithm>

#include "hwe.h"
#include "func.h"

using namespace std;


Matrice UG;
Matrice UGcoord; 
Matrice UGt;
Matrice Voisin; 
Matrice Poids;
Matrice Poids_total;
Matrice Espace(0,200,0,200);
Matrice Espacet;
Matrice Pente;
Vecteur Pente_obs;
Vecteur Statsup_Pente;
Matrice Ind;
Matrice data;
Matrice data_sauve;
Matrice data2;
Matrice Homozygotes;
Matrice Heterozygotes;
Matrice Gradient_allele;
Vecteur Alleles_nuls;
Vecteur Modalite;
Vecteur Total_petits_effectifs;
Matrice Nbgenes;//contient pour chaque UG le nombre total de genes par locus
int nombre_UG;
int nbr;
int nombre_alleles;
double minX,maxX,minY,maxY;
Vecteur allele;
int Nombre_min_voisin;
Matrice frequence_alleles;
double denominateur;
Matrice proba;
int effectif_min;
double rayon;
Vecteur Corres;
int nombre_locus,nombre_individus;
double minpente,maxpente;
int sortie;
char polcar;
char fic_data[100];
int nb_repetitions;

//int MAX_ALLELE=20; nombre max d'alleles differents au meme locus defini
Matrice Matrice_occurence;
int a[LENGTH], n[MAX_ALLELE];
double ln_p_observed, ln_p_simulated, p_mean, p_square;
double constant, p_simulated, total_step;
int no_allele, total, counter, actual_switch;
Index indexf;
struct randomization sample;
struct outcome result;
FILE *infile, *outfile;
long t1;


void extraction_data()
//extrait les positions et les valeurs des loci 
{
int i,j;
char nom_ind[10];
char reponse;
cout<< endl;
ifstream f(fic_data);
ofstream g("noms_individus");
cout<<"Est ce que vos donnees contiennent un champ identificateur de l'individu ? (o/n) : ";
 cin>>reponse;    
for (i=0;i<nombre_individus;i++)
   {  
     if (reponse=='o') f>>nom_ind;
        g<<i<<"   "<<nom_ind<<endl;
     for (j=0;j<2+2*nombre_locus;j++)
       {
	 f>>data[i][j];
       }
   }
}

void cherche_minmax ()
//retourne les valeurs minX,maxX,minY et maxY correspondant aux valeurs min
//et max des deux premieres colonnes du tableau tab (ie latitude, longitude)
{

int i=0;

minX=data[0][0];
maxX=minX;
minY=data[0][1];
maxY=minY;
for (i=1;i<nombre_individus;i++)
{
if (data[i][0]<minX)
   minX=data[i][0];
else
   if (data[i][0]>maxX) maxX=data[i][0];
}
for (i=1;i<nombre_individus;i++)
{
if (data[i][1]<minY) minY=data[i][1];
        else
   if (data[i][1]>maxY) maxY=data[i][1];
   }
cout<<"minX="<<minX<<" maxX="<<maxX<<" minY="<<minY<<" maxY="<<maxY<<endl;
}

double distancekm (int u1, int u2) //renvoie la distance en km entre les deux UGs
//u1 et u2 du tableau UG -- mais attention UG est indicee de 0 a nombre_UG-1 alors
//que u1 et u2 vont de 1 a nombre_UG

{
  double temp,z,L1,l1,L2,l2;
  L1=UGcoord[u1-1][0]; //longitude = X 
  l1=UGcoord[u1-1][1]; // latitude = Y
  L2=UGcoord[u2-1][0];
  l2=UGcoord[u2-1][1];
  z=sin(l2)*sin(l1)*cos(L1-L2);
  z=z+(cos(l1)*cos(l2));

  temp=6400*acos(z);
  //cout<<"distance entre les UG "<<u1-1<<" et "<<u2-1<<" : "<<temp<<" km"<<endl;
   return temp;
  }



double distancekm_cart (int u1, int u2) //renvoie la distance en km entre les
//deux UGs u1 et u2 du tableau UG -- mais attention UG est indicee de 0 a
//nombre_UG-1 alors que u1 et u2 vont de 1 a nombre_UG
{
  double temp,z,x,xp,y,yp;
  x=UGcoord[u1-1][0]; //longitude = X 
  y=UGcoord[u1-1][1]; // latitude = Y
  xp=UGcoord[u2-1][0];
  yp=UGcoord[u2-1][1];
  
  z=(y-yp)*(y-yp);
  z=sqrt((x-xp)*(x-xp)+z);

  temp=z;
  return temp;
}


void unite_geographique_cart (double R,double d,int effmin)  //effmin: effectif
//min dans le cercle
//Met en place la grille, ainsi que les unites geographiques (UG) (cercle de
//rayon R)
//La grille est d'unite la distance d des UG

//on attribue a chaque individu ses UG dans le tableau individu
//X est la coordonnee selon l'axe 0 (<=> longitude), Y la coordonnees selon l'axe
//1 (<=> latitude)
{
int i=0,j=0,k=0,ind=0;
int temp1=0; //indice l'unite geographique
int temp2=0;
int nbre_div=0;
double X=0,Y=0,z=0,total=0;
char fin='N';
Vecteur N;
Vecteur M;


cherche_minmax();

//Les tableaux  sont initialises dans le main

X=minX;
Y=minY;
i=0;j=0;

do //Pour chaque unite geographique
{
        temp1=i+j*nbre_div;
        UGt[temp1][0]=X;
        UGt[temp1][1]=Y;
        for (k=0;k<nombre_individus;k++) //Pour chaque individu
        {
	  z=(data[k][0]-X)*(data[k][0]-X);
	  z=z+(data[k][1]-Y)*(data[k][1]-Y);
	  
        if (z<=(R*R))
          {
            UGt[temp1][2]= UGt[temp1][2]+1; //Nombre d'individus appartenant au
//cercle de rayon R
            temp2= 2+int(UGt[temp1][2]);
            UGt[temp1][temp2]= k; //On inscrit l'individu dans le tableau UG
          }
        }
        if (X<maxX)
// tant que X est plus petit que Xmax, on l'incremente - l'increment tient compte
//de
// la latitude du centre de l'UG precedente
        {
    
          Espacet[i][j]=temp1+1; //on ajoute 1 pour ne pas confondre le 0 de
//l'init et le 0 de l'UG 0	  
          i=i+1;
          X=X+d;
	 
        }
        else
//on incremente Y ie la latitude et on reinitialise la longitude a la valeur min
        {
          j=j+1;
          nbre_div=i+1;
          i=0;
          Y=Y+d;
          X=minX;

        }

//somme des individus appartenant a ch UG (doit etre sup a nombre_individus
total=total+UGt[temp1][2];

if (Y>maxY && X>maxX) fin='O';
}
while (fin=='N');

 
//construction du tableau UGcoord dont chaque UG contient au moins effmin individus
 
ind=0; 
for (i=0;i<temp1;i++)
   if (UGt[i][2]>=effmin)    
     ind++;
     
nombre_UG=ind;
cout<<"With your data and parameters, the number of spatial zone is : "<<nombre_UG<<endl;

UGcoord.Init(0,nombre_UG,0,3+nombre_individus); // idem UG mais sans colonnes alleles
Corres.Init(0,2500);
ind=0;
for (i=0;i<temp1;i++) // avant c'etait temp-1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   if (UGt[i][2]>=effmin)   
     {     
       Corres[i+1]=ind+1;
       UGcoord[ind]=UGt[i];
       ind++;
     }


//On construit le tableau espace � partir de espacet et de Corres pour ne retenir
//que les UG qui contiennent effmin individus

 int temp4;
 for (i=0;i<100;i++)
   {
   for (j=0;j<100;j++)
     {
       temp4=int(Espacet[i][j]);
       Espace[i][j]=Corres[temp4];
      
     }
   }

Voisin.Init(1,nombre_UG,0,8);
Poids_total.Init(1,nombre_UG,0,8);
Poids.Init(1,nombre_UG,0,8); //contient pour chaque UG la distance (en km) entre
//le centre de l'UG consid�r�e et chacun de ses huit voisins.
//remplissage du tableau voisin et du tableau Poids
//Cas general et cas particuliers (bords). Attention le tableau voisin est indic�
//de 1 � 92!!!

 for (i=0;i<100;i++)
   for (j=0;j<100;j++)
     if (Espace[i][j]>0)
     { 
       temp4=int(Espace[i][j]);
       if (j>0)
	 {
	   if (Espace[i][j-1] >0) 
	     {
       	       Poids[temp4][0]=1/distancekm_cart(temp4,int(Espace[i][j-1]));
	      
	       Voisin[temp4][8]++;
	     }
	   Voisin[temp4][0]=Espace[i][j-1];
	 }

       if ((j>0) && (i<100-1))
	 {
	   if (Espace[i+1][j-1] >0) 
	     {
	       Poids[temp4][1]=1/distancekm_cart(temp4,int(Espace[i+1][j-1]));
	      
	       Voisin[temp4][8]++;
	     }
	   Voisin[temp4][1]=Espace[i+1][j-1];
	 }
       
       if ((i<100-1) && (j<100-1))
	 {
	   if (Espace[i+1][j+1] >0) 
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][2]=1/distancekm_cart(temp4,int(Espace[i+1][j+1]));
	      
	     }
	   Voisin[temp4][2]=Espace[i+1][j+1];
	 }

       if (i<100-1)
	 {
	   if (Espace[i+1][j] >0) 
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][3]=1/distancekm_cart(temp4,int(Espace[i+1][j]));
	     }
	   Voisin[temp4][3]=Espace[i+1][j];
	 }

       if ((i>0) && (j<100-1))
	 {
	   if (Espace[i-1][j+1] >0) 
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][4]=1/distancekm_cart(temp4,int(Espace[i-1][j+1]));
	     }
	   Voisin[temp4][4]=Espace[i-1][j+1];
	 }

       if (i>0)
	 {
	   //	   if (Espace[i-1][j]-1 >0) pourquoi ????
	   if (Espace[i-1][j] >0)
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][5]=1/distancekm_cart(temp4,int(Espace[i-1][j]));
	     }
	   Voisin[temp4][5]=Espace[i-1][j];
	 }

       if ((i>0) && (j>0))
	 {
	   //	   if (Espace[i-1][j-1]-1 >0) pourquoi ???
	   if (Espace[i-1][j-1] >0)
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][6]=1/distancekm_cart(temp4,int(Espace[i-1][j-1]));
	     }
	   Voisin[temp4][6]=Espace[i-1][j-1];
	 }

       if (j<100-1)
	 {
	   //	   if (Espace[i][j+1]-1 >0) pourquoi ???
	   if (Espace[i][j+1] >0)
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][7]=1/distancekm_cart(temp4,int(Espace[i][j+1]));
	     }
	   Voisin[temp4][7]=Espace[i][j+1];
	 }
     }


}

void unite_geographique_pol (double R,double d,int effmin)  //effmin: effectif
//min dans le cercle
//Met en place la grille, ainsi que les unites geographiques (UG) (cercle de
//rayon R)
//La grille est d'unite la distance d des UG

//on attribue a chaque individu ses UG dans le tableau individu
//X est la longitude, Y la lattitude
{
int i=0,j=0,k,ind;
int temp1; //indice l'unite geographique
int temp2;
int nbre_div=0;
double X,Y,z,cosR,total,temp;
char fin='N';

Vecteur M;

 for (i=0;i<nombre_individus;i++)
   {
//transformation des coord exprimees en degre, en radian
data[i][0]=(M_PI/180)*data[i][0];
data[i][1]=(M_PI/180)*data[i][1];
//transformation de la latitude (deplacement de l'origine)
data[i][1]=(M_PI/2)-data[i][1];
//cout<<"long"<<data[i][0]<<"   et lat  "<<(M_PI/2)-data[i][1]<<endl;
   }
//on intervertit minY et maxY parce que le changement de coordonnees (deg en rad)
//rend le min max et vice versa

temp=minY;minY=maxY;maxY=temp;
minX=(M_PI/180)*minX;
minY=(M_PI/2)-(M_PI/180)*minY;
maxX=(M_PI/180)*maxX;
maxY=(M_PI/2)-(M_PI/180)*maxY;
 
i=0;
X=minX;
Y=minY;
cosR=cos(R/6400);

do //Pour chaque unite geographique
{
        temp1=i+j*nbre_div;
        UGt[temp1][0]=X;
        UGt[temp1][1]=Y;
        for (k=0;k<nombre_individus;k++) //Pour chaque individu
        {
	  z=sin(Y)*sin(data[k][1])*cos(data[k][0]-X);
	  z=z+(cos(data[k][1])*cos(Y));
	  //z=cos(Y)*cos(data[k][1])*cos(data[k][0]-X);
	  //z=z+(sin(data[k][1])*sin(Y));

        if (z>=cosR)
          {
            UGt[temp1][2]= UGt[temp1][2]+1; //Nombre d'individus appartenant au
//cercle de rayon R
            temp2= 2+int(UGt[temp1][2]);
            UGt[temp1][temp2]= k; //On inscrit l'individu dans le tableau UG
          }
        }
        if (X<maxX)
// tant que X est plus petit que Xmax, on l'incremente - l'increment tient compte
//de
// la latitude du centre de l'UG precedente
        {
    
          Espacet[i][j]=temp1+1; //on ajoute 1 pour ne pas confondre le 0 de
//l'init et le 0 de l'UG 0	  
          i=i+1;
          X=X+(d/6400)*(1/sin(Y));
	  //	  cout <<"UG(X)  "<<X<<endl;
        }
        else
//on incremente Y ie la latitude et on reinitialise la longitude a la valeur min
        {
          j=j+1;
          nbre_div=i+1;
          i=0;
          Y=Y+(d/6400);
          X=minX;
	  //        cout <<"UG(Y)  "<<Y<<endl;
	  //  cout<<"appuyer sur une touche pour continuer ";
	  //  cin>>attend;

        }

//somme des individus appartenant a ch UG (doit etre sup a nombre_individus
total=total+UGt[temp1][2];

if (Y>maxY && X>maxX) fin='O';
}
while (fin=='N');

 
//construction du tableau UG dont chaque UG contient au moins effmin individus
 
ind=0; 
for (i=0;i<temp1;i++)
   if (UGt[i][2]>=effmin)    
     ind++;
     
nombre_UG=ind;
cout<<"nombre UG = "<<nombre_UG<<endl;

//UG.Init(0,nombre_UG,0,3+nombre_individus+500);
UGcoord.Init(0,nombre_UG,0,3+nombre_individus);
Corres.Init(0,2500);
 char attend;
 ind=0;
for (i=0;i<temp1-1;i++)
   if (UGt[i][2]>=effmin)   
     { 
       Corres[i+1]=ind+1;
       UGcoord[ind]=UGt[i];
       ind++;
     }

//cout<<"appuyer sur une touche pour continuer ";
//cin>>attend;
//On construit le tableau espace � partir de espacet et de Corres ppur ne retenir
//que les UG qui contiennent effmin individus

 int temp4;
 for (i=0;i<100;i++)
   {
   for (j=0;j<100;j++)
     {
       temp4=int(Espacet[i][j]);
       Espace[i][j]=Corres[temp4];
       // if (Espace[i][j]>0)  cout<<Espace[i][j]<<"  ";
     }
   }
Voisin.Init(1,nombre_UG,0,8);
Poids_total.Init(1,nombre_UG,0,8);
Poids.Init(1,nombre_UG,0,8); 

//contient pour chaque UG la distance (en km) entre
//le centre de l'UG consid�r�e et chacun de ses huit voisins.
//remplissage du tableau voisin et du tableau Poids
//Cas general et cas particuliers (bords). Attention le tableau voisin est indic�
//de 1 � 92!!!

 for (i=0;i<100;i++)
   for (j=0;j<100;j++)
     if (Espace[i][j]>0)
     { 
       temp4=int(Espace[i][j]);
       if (j>0)
	 {
	   if (Espace[i][j-1] >0) 
	     {
     	       Poids[temp4][0]=1/distancekm(temp4,int(Espace[i][j-1]));
	       Voisin[temp4][8]++;
	     }
	   Voisin[temp4][0]=Espace[i][j-1];
	 }

       if ((j>0) && (i<100-1))
	 {
	   if (Espace[i+1][j-1] >0) 
	     {
	       // cout<<"temp4="<<temp4<<endl;
	       // cout<<"dist="<<distancekm(temp4,int(Espace[i+1][j-1]))<<endl;
	       Poids[temp4][1]=1/distancekm(temp4,int(Espace[i+1][j-1]));
	       Voisin[temp4][8]++;
	     }
	   Voisin[temp4][1]=Espace[i+1][j-1];
	 }
       
       if ((i<100-1) && (j<100-1))
	 {
	   if (Espace[i+1][j+1] >0) 
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][2]=1/distancekm(temp4,int(Espace[i+1][j+1]));
	     }
	   Voisin[temp4][2]=Espace[i+1][j+1];
	 }

       if (i<100-1)
	 {
	   if (Espace[i+1][j] >0) 
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][3]=1/distancekm(temp4,int(Espace[i+1][j]));
	     }
	   Voisin[temp4][3]=Espace[i+1][j];
	 }

       if ((i>0) && (j<100-1))
	 {
	   if (Espace[i-1][j+1] >0) 
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][4]=1/distancekm(temp4,int(Espace[i-1][j+1]));
	     }
	   Voisin[temp4][4]=Espace[i-1][j+1];
	 }

       if (i>0)
	 {
	   //	   if (Espace[i-1][j]-1 >0) pourquoi ????
	   if (Espace[i-1][j] >0)
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][5]=1/distancekm(temp4,int(Espace[i-1][j]));
	     }
	   Voisin[temp4][5]=Espace[i-1][j];
	 }

       if ((i>0) && (j>0))
	 {
	   //	   if (Espace[i-1][j-1]-1 >0) pourquoi ???
	   if (Espace[i-1][j-1] >0)
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][6]=1/distancekm(temp4,int(Espace[i-1][j-1]));
	     }
	   Voisin[temp4][6]=Espace[i-1][j-1];
	 }

       if (j<100-1)
	 {
	   //	   if (Espace[i][j+1]-1 >0) pourquoi ???
	   if (Espace[i][j+1] >0)
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][7]=1/distancekm(temp4,int(Espace[i][j+1]));
	     }
	   Voisin[temp4][7]=Espace[i][j+1];
	 }
     }
 

}



void creation_tableau_Ind()
{
Vecteur N;
int i,j,k,No;
//on cree le tableau inverse Individus qui contiendra les references
//des UG auxquelles il appartient

N.Init(0,nombre_individus);
//pour chaque UG

for (k=0;k<nombre_UG;k++)
  if (UGcoord[k][2]!=0)
        {
  for (j=0;j<UGcoord[k][2];j++) //pour chaque individu inscrit dans cette UG
    {
    i=int(UGcoord[k][3+j]);//i est donc la ref de l'individu contenu dans cette UG
    N[i]=N[i]+1;//N est un compteur pour chaque ind du nom d'UG auquelles il
//appartient
    No=int(N[i]);
    Ind[i][No]=k;
    
    }
  }

//nombre_UG=temp1; //variable donnant le nombre total d'UG

//et enfin on sait a combien d'UG appartient l'individu
UGcoord[nombre_UG][2]=nombre_individus;
for (i=0;i<nombre_individus;i++)
{
  Ind[i][0]=N[i];
  if (N[i]==0)
    { 
      if (nbr==0) cout<<"Pay attention : Individual "<<i<<" is present in any Spatial Zone "<<endl<<flush;
      UGcoord[nombre_UG][2]--;
    }
}
}

void recodage_donnees_format_Pierre()
  //recode les donnees genetiques suivant le systeme suivant : 0..n pour les n
//premiers alleles
  // du premier locus, n+1...m pour les m-n-1 alleles du locus suivant etc ...
{

  int i,j,k,k1,k2,n;
  Vecteur V;
  Vecteur V2; //V2 : vecteur intermediaire qui superpose les 2 alleles du meme
//locus 
  Vecteur ordre;
  Vecteur newV;//vecteur intermediare recodde 
  
  newV.Init(0,2*nombre_individus-1);
  V2.Init(0,2*nombre_individus-1);
  int indice=0;
  data2=data;
  for (i=2;i<=nombre_locus*2;i=i+2)  //pour chaque locus
  
    {//cr�ation du vecteur V2
      V=data.Extrait_col(i);
      for (k=0;k<nombre_individus;k++)
        V2[k]=V[k];
      V=data.Extrait_col(i+1);
      for (k=0;k<nombre_individus;k++)
        V2[k+nombre_individus]=V[k];
      //recodage
      ordre=V2.Ordre_croissant(); //ordre est un vecteur rang
      int ii=0;
      char fin='n';

     while ((V2[ordre[ii]]<0) && (fin='n'))
	{
	  ii=ii+1;
	  fin='o';
	}
	newV[ordre[ii]]=indice;

      for (j=0;j<ii;j++)
	newV[int(ordre[j])]=-1; //cas d'une valeur manquante, elle n'est pas recod�e
      for (j=ii;j<2*nombre_individus-1;j++)
        {
          k1=int(ordre[j]);
          k2=int(ordre[j+1]);
          if (V2[k2]==V2[k1]) 
             newV[k2]=newV[k1];
          else {newV[k2]=newV[k1]+1;indice=int(newV[k2]);}  
        }

      n=(i-2)/2;
      allele[n]=indice;

      //on range les vecteurs codes dans data
             
      for (k=0;k<nombre_individus;k++)
        V[k]=newV[k];
      data.Copie_col(V,i);
      for (k=0;k<nombre_individus;k++)
         V[k]=newV[k+nombre_individus];
       data.Copie_col(V,i+1); 
       
      indice=indice+1;
      
  }
      nombre_alleles=indice; //nombre total d'alleles=nombre_alleles
} 

  void calcul_frequences_alleliques_par_UG()
{
  int i=0,j=0,k=0,l=0,m=0;
  int a=0;

  //initialisation des colonnes nombre_individus+3 � .... de UG
  for (i=0;i<=nombre_UG;i++)
    for (j=nombre_individus+3;j<3+nombre_individus+nombre_alleles;j++)
      UG[i][j]=0;
  
    for (i=0;i<nombre_UG;i++)
	{
	  for (j=0;j<UG[i][2];j++) //pour chaque individu de cet UG
        {
          k=int(UG[i][j+3]);//k est l'indice de l'individu dans les tab data
          for (l=0;l<nombre_locus*2;l++)
            { 
              m=int(data[k][l+2]);//code de l'allele au locus l de l'individu k,
//ce code est compris
                             //entre 0 et nombre_alleles, le (+2) est du au fait
//que les deux
                             //premieres colonnes de data contiennent ses coord
//geographiques 
	      if (m>=0)
		{
		  UG[i][nombre_individus+3+m]++;
	       
		}
         }
        }
	  int ind;
	  for (l=0;l<nombre_locus;l++)
	    for (k=0;k<UG[i][2];k++)
	    {
	      ind=int(UG[i][2+k]);
	      m=int(data[ind][2*l+2]);//on ne teste qu'un allele sur 2, car on considere que si un est manquante, l'autre l'est forcemment!!!!
	      if (m>=0)
		Nbgenes[i][l]++;//Nombre de genes a chaque loci dans chaque UG
	    } }
      
    //calcul du numbre total d'occurence de chaque allele, on range cette donnee
//dans le tableau
    //UG a la ligne nombre_UG, on calcul aussi le nombre total d'alleles pour
//chaque UG, on range
    //cette donnee dans le tableau UG  a la colonne
//nombre_individus+3+nombre_allele

    for (i=0;i<nombre_UG;i++)
      {
      for (j=nombre_individus+3;j<nombre_individus+3+nombre_alleles;j++)
       
UG[i][nombre_individus+3+nombre_alleles]=UG[i][nombre_individus+3+nombre_alleles]+UG[i][j];
      }

    for (i=0;i<nombre_individus;i++)//pour tous les individus
      for (j=0;j<2*nombre_locus;j++) //pour tous les alleles
	{
	  a=int(data[i][j+2]); //code de l'allele au locus j de l'individu i
         
UG[nombre_UG][nombre_individus+3+a]=UG[nombre_UG][nombre_individus+3+a]+1;
	}
  
}

void affiche_totaux()
{
  int i,j;
cout<<"Occurences des alleles dans l'ensemble de la population :"<<endl;
  for (j=nombre_individus+3;j<nombre_individus+3+nombre_alleles;j++)
      cout<<"Allele : "<<j-nombre_individus-3<<"   occurence :"<<UG[nombre_UG][j]<<endl;
  cout<<endl;
  cout<<"Nombre total d'alleles par UG :"<<endl;
  for (i=0;i<nombre_UG;i++)
    cout <<"UG : "<<i<<" nombre total :"<<UG[i][nombre_individus+3+nombre_alleles]<<"  soit "<<UG[i][nombre_individus+3+nombre_alleles]/(nombre_locus*2)<<" individus"<<endl;
}
        

void affiche_UG_numero(int num)//Affiche pour chaque UG l'effectif des individus
//et surtout l'occurence 
  //des alleles dans chaque UG pour chaque locus
{
  int k;
  double total=0;

  if (UG[num][2]==0)
    cout<<"l'UG "<<num<<" est vide "<<endl;
  else
    {
      cout<<"L'UG "<<num<<" contient "<< UG[num][2]<<" individus , les frequences alleliques sont "<<endl;
      for (k=nombre_individus+3;k<nombre_individus+3+nombre_alleles;k++) //pour
//tous les alleles
        {
        cout<<"code allele "<< k-nombre_individus-3<< " frequence "<< UG[num][k]<<endl;
      total=total+ UG[num][k];
        }
    }
  cout<<"nombre total d'alleles : "<<total<<endl;
}

void recalcul_frequences_alleliques(int num_ind)
//recalcule des frequences alleliques en enlevant l'individu des UG auquelles il
  //appartient.
{
  int k=0,l=0,ug=0;
  double m=0;
  for (k=0;k<Ind[num_ind][0];k++) //pour toutes les UG auquelles appartient cet
//individu
    {
    for (l=0;l<nombre_locus*2;l++) //pour tous les alleles de cet individu
      {
        m=data[num_ind][l+2]; //m est le code de l'allele, m varie entre -1 et
//nombre_alleles-1
        ug=int(Ind[num_ind][k+1]); //ug est le numero de l'UG
        if (m>=0) frequence_alleles[ug][m]--; //on retire l'allele de l'ind du nombre
//d'occurences lorsque la donn�e n'est pas manquante
      }
    }
}

void construit_tableau_modalite()
{
 int locus,al;

  //avant de caculer la proba, on construit un tableau modalite qui contient le
//nombre d'allele different pour chaque locus
 
  for (al=0;al<=allele[0];al++)
    Modalite[al]=allele[0]+1;
  for(locus=1;locus<nombre_locus;locus++)
    for (al=int(allele[locus-1])+1;al<=int(allele[locus]);al++)
      Modalite[al]=allele[locus]-allele[locus-1];
}

void proba_rannala(int i)
  //calcule la proba multilocus  du genotype d'un individu dans les UG
{
  int j=0,k=0,al1=0 ,al2=0; 
  double temp1=0,temp2=0,p=0;
 

   for (j=0;j<nombre_UG;j++)
      {
	proba[j][i]=1;
	for(k=0;k<nombre_locus*2-1;k=k+2)
	{
	  int gene=int(k/2);
	  temp1=1/((2*Nbgenes[j][gene]+2)*(2*Nbgenes[j][gene]+1));
	  al1=int(data[i][k+2]);
	  al2=int(data[i][k+3]);
	  temp2=1/Modalite[int(k/2)];
	  if ((al1>=0) && (al2>=0))
	    {
	  if (al1==al2)
	   
p=temp1*(frequence_alleles[j][al1]+temp2+1)*(frequence_alleles[j][al1]+temp2); 
	  else
	   
p=2*temp1*(frequence_alleles[j][al1]+temp2)*(frequence_alleles[j][al2]+temp2);
	  proba[j][i]=proba[j][i]*p;
	}
	}

      }

}


void calcul_proba_genotype_par_ours(int i)
//calcul de la probabilite theorique (hyp HW) du genotype de l'ours i pour chaque
//UG selon la m�thode de Paetkau
  //affiche la vraisemblance (-log p)
{
  int j,k,kp,l;
  double m1,m2;
  
  for (j=0;j<nombre_UG;j++)
  {
  proba[j][i]=1;
  for (k=0;k<nombre_locus*2-1;k=k+2) //pour chaque allele de l'individu
      {      
        m1=data[i][k+2];//m1 num�ro de l'all�le 1
        m2=data[i][k+3];
        if (m1==m2)
          {
            for (l=0;l<Ind[i][0];l++) //pour toutes les UG auxquelles appartient
//cet ind
              if (Ind[i][l]==j) //si l'ind appartient a l'UG
              {
                kp=k/2;
                Homozygotes[j][kp]++;
              }
           
proba[j][i]=proba[j][i]*frequence_alleles[j][m1]*frequence_alleles[j][m2];
              }
        else 
	  {
	    for (l=0;l<Ind[i][0];l++) //pour toutes les UG auxquelles appartient cet
//ind
            if (Ind[i][l]==j) //si l'ind appartient a l'UG
              {
                kp=k/2;
                Heterozygotes[j][kp]++;
	      }
	   
proba[j][i]=2*proba[j][i]*frequence_alleles[j][m1]*frequence_alleles[j][m2];
	  }
      }
  }     
}

void construction_matrice_freq_all()//range les frequences dans un tableau
{
  int i,j;
 
  for (i=0;i<nombre_UG;i++)
    {
    for (j=0;j<nombre_alleles;j++)
      {
      frequence_alleles[i][j]=UG[i][j+3+nombre_individus];
  
      }
    }
}

void genere_fichiers_freq_alleles()
{
int i,k;
  int entier;
  char nom_fic[10];
  ifstream f("noms_alleles");
  for (i=0;i<nombre_alleles;i++)
    {
      f>>entier;
      f>>nom_fic;
      ofstream g(nom_fic);
      for (k=0;k<nombre_UG;k++)
	{
	  g<<k<<" , "<<UG[k][0]*(180/M_PI)<<" , "<<(M_PI/2 - UG[k][1])*(180/M_PI)<<" , "<<frequence_alleles[k][i]/(2*UG[k][2])<<endl;
	}
    }

}

void genere_un_seul_fichier_freq_allele()
{
int i,k;

  ofstream g("freq_alleles.txt");
  for (k=0;k<nombre_UG;k++)
    {
   g<<k<<" , "<<UG[k][0]*(180/M_PI)<<" , "<<(M_PI/2 - UG[k][1])*(180/M_PI)<<" ,";
  for (i=0;i<nombre_alleles;i++)
   g<<frequence_alleles[k][i]/(2*UG[k][2])<<" , ";
  g<<endl;
   }
}




void genere_fichiers_proba_ours(int i)
  //genere autant de fichiers que d'individus - le nom du fichier est le nom de l'individu
{ 
  
  int k;
  char nom_fic[20];
  cout<<"Give a filename for the individual "<<i<<" probability file : ";
  //cin>>fichier;
  cin>>nom_fic;
  // ofstream g(fichier);
  ofstream g(nom_fic);
  for (k=0;k<nombre_UG;k++)
    {
    g<<k<<" , "<<UG[k][0]<<" , "<<UG[k][1]<<", "<<-log10(proba[k][i])<<endl;
    }
}



void genere_un_seul_fichier_proba_ours()
  //genere un fichier de format : num_ug,x,y,proba_ours1,proba_ours2,...
  // proc�dure non utilis�e - pour polaire
{
  int k,i;
  ofstream g("probas_ours.txt");
  for (k=0;k<nombre_UG;k++)
    {
      g<<k<<" , "<<UG[k][0]*(180/M_PI)<<" , "<<(M_PI/2 - UG[k][1])*(180/M_PI)<<",";
      for (i=0;i<nombre_individus;i++)
	g<<-log10(proba[k][i])<<" , ";
      g<<endl;
    }
}

   
void calcul_la_pente()
  //calcule la pente moyenne par UG pour tous les individus
  //les valeurs pour les voisins sont pond�r�es par la distance entre les voisins
{
int i=0,j=0,k=0;
double temp=0;
double totalpoids=0;
Nombre_min_voisin=6;
  for (i=1;i<nombre_UG;i++)
      for (k=0;k<8;k++)
	Poids_total[i][k]=Poids[i][k];

k=0;
for (i=0;i<nombre_individus;i++)
  for (j=0;j<nombre_UG-1;j++)
{
  if (Voisin[j+1][8]>=Nombre_min_voisin)
    {
      for (k=0;k<8;k++)
	{
	  if (Voisin[j+1][k]!=0)  //si ce voisin existe
	    {
	      // 
temp=temp+Poids_total[j+1][k]*fabs(-log10(proba[j][i])+log10(proba[(Voisin[j+1][k]-1)][i]));
	     
temp=temp+fabs(-log10(proba[j][i])+log10(proba[(Voisin[j+1][k]-1)][i]));
	      totalpoids=totalpoids+Poids_total[j+1][k];
	    }
	}
      Pente[i][j]=temp/Voisin[j+1][8];//(totalpoids);
      temp=0;totalpoids=0;
    }
  }


   
     for (j=0;j<nombre_UG-1;j++)
       {
        for (i=0;i<nombre_individus;i++) 
	  Pente[nombre_individus][j]=Pente[nombre_individus][j]+ Pente[i][j];
  
	Pente[nombre_individus][j]=Pente[nombre_individus][j]/nombre_individus;
       }

//calcul de la variance sur la pente

double ecart,ecart2,sommeecarts;

      for (j=0;j<nombre_UG-1;j++)
	{
	  sommeecarts=0;
	 for (i=0;i<nombre_individus;i++)
	   { 
	     ecart=Pente[nombre_individus][j]-Pente[i][j];
	     ecart2=ecart*ecart;
	     sommeecarts=sommeecarts+ecart2;
	   }
	 Pente[nombre_individus+1][j]=sommeecarts/(nombre_individus-1);
	 Pente[nombre_individus+1][j]=sqrt(Pente[nombre_individus+1][j]);
    
       }

//recherche des valeurs min et max de la pente
     minpente=1000;maxpente=0;
     for (i=0;i<nombre_UG;i++)
       if (Pente[nombre_individus][i]<minpente)
minpente=Pente[nombre_individus][i];
       else if (Pente[nombre_individus][i]>maxpente)
maxpente=Pente[nombre_individus][i];

}  

void calcul_gradient_freq_alleliques()
{
int i=0,j=0,k=0;
int indice;
double temp=0;
double totalpoids=0;

//----  Dessin------- 
  
////TCanvas *c2 = new TCanvas("c2", "ours",5);
////c2->DrawFrame(minX,minY,maxX,maxY,"Gradient des frequences alleliques");  
//dessin d'axes, et ajuste Range automatiquement

for (i=0;i<nombre_UG;i++)
  if (Voisin[i+1][8]>=Nombre_min_voisin)
  {
    for (j=0;j<nombre_alleles;j++)
    {
      for (k=0;k<8;k++)
      {
	if (Voisin[i+1][k]!=0)  //si ce voisin existe
        {
	  indice=int(Voisin[i+1][k]);
	 
temp=temp+Poids[i+1][k]*fabs(frequence_alleles[i][j]-frequence_alleles[indice-1][j]);
	  totalpoids=totalpoids+Poids[i+1][k];
	
	}
      }
    Gradient_allele[i][j]=temp/totalpoids;
    temp=0;totalpoids=0;
  }
}  
  
for (j=0;j<nombre_UG;j++)
  {
  for (i=0;i<nombre_alleles;i++) 
   Gradient_allele[j][nombre_alleles]=Gradient_allele[j][nombre_alleles]+ Gradient_allele[j][i];

  
Gradient_allele[j][nombre_alleles]=Gradient_allele[j][nombre_alleles]/nombre_alleles;


  }
}
    
void coupe_selon_longitude()

{
char ouinoncoupe,ok2;
int i,i2,minD=1000,ind_min=0;
double coupeX;
char fic2[30];
Vecteur D;
D.Init(0,nombre_UG);

//coupe dans le plan selon une longitude

 cout<<"voulez-vous une coupe dans l'espace des pentes selon la longitude (o/n) ?";
 cin>>ok2;
 do
  {
   if (ok2=='o')
     {
       cout<<"Pour quelle valeur de la longitude voulez-vous une coupe ? ";
       cin>>coupeX;  
       ////if (coupeX<9999) {cout<<"Fichier (1) ou graphique (2) ? (1/2) :" ;
       sortie=1;}
   ////if (coupeX==9999) sortie=2;
       
   //On doit rechercher la valeur de la longitude la plus proche dans la tableau
//UG , soit D vecteur  diff�rence longitude rentr�� longitude UG 
   //On recherche Dmin

       if (coupeX<9999) for (i2=0;i2<nombre_UG;i2++)
	 {
	   D[i2]=fabs(UG[i2][0]-coupeX);
	   if (D[i2]<minD)
	     {minD=int(D[i2]);ind_min=i2;}
	 }
       
	 { 
	 cout<<"Entrez un nom de fichier : ";
	 cin>>fic2;
	 ofstream g(fic2);
	 for (i2=0;i2<nombre_UG;i2++)
	 {
	   if (UG[i2][0]==UG[ind_min][0])
	     g<<UG[i2][1]; g<<"   ";g<<Pente[nombre_individus][i2];g<<endl;
	 ;}
	 }
  
   cout<<"Encore ? ";
   cin>>ouinoncoupe;
  }
  while (ouinoncoupe=='o');
}

//void genere_fichier_pente()
void genere_fichier_pente_cart()
{
//Ecriture du fichier r�sultats / pente
//colonne 1 : longitude
//colonne 2 : latitude
//colonne 3 : pentes
//colonne 4 : pcalc

int i;
char nom_fic[20];

cout<<"Enter a name for the slopes file : ";
cin>>nom_fic;
ofstream g(nom_fic);

for (i=0;i<nombre_UG;i++)
     {
       g<<UG[i][0]<<"    "<<UG[i][1]<<"    "<<Pente_obs[i]<<"   ";
       if (Statsup_Pente[i]<0) Statsup_Pente[i]=-1; 
       if (nb_repetitions>1) g<<Statsup_Pente[i]<<"   ";
       g<<endl;
     }                               

}
void genere_fichier_pente_pol()
  //tranformation des coordonne en degre et pi/2-latitude
{
//Ecriture du fichier r�sultats / pente
//colonne 1 : longitude
//colonne 2 : latitude
//colonne 3 : pentes
//colonne 4 : pcalc

int i;
char nom_fic[20];

cout<<"Enter a name for the slopes file : ";
cin>>nom_fic;
ofstream g(nom_fic);

for (i=0;i<nombre_UG;i++)
     {
       g<<UG[i][0]*180/M_PI<<"    "<<(M_PI/2-UG[i][1])*180/M_PI<<"    "<<Pente_obs[i]<<"   ";
       if (Statsup_Pente[i]<0) Statsup_Pente[i]=-1; 
       if (nb_repetitions>1) g<<Statsup_Pente[i]<<"   ";
       g<<endl;
     }                               

}




/*
=========================================================================================

                     FISHER

=========================================================================================
*/

void read_data (int a[LENGTH], int *no_allele, int *total,struct randomization *sample)
  /*
int a[LENGTH];
int *no_allele, *total;
struct randomization *sample;*/

{
  register int i, j, l;

  *total = 0;

 
  *no_allele=MAX_ALLELE;
 

  for ( i = 0; i < *no_allele; ++i ) {
    for ( j = 0; j <= i; ++j ) {
	 l = LL(i, j);
	 a[l]=int(Matrice_occurence[i][j]);
	 *total += a[l];
    }
  }

  sample->step=2000;
  sample->group=1000;
  sample->size=1000;

}

void print_data (int a[LENGTH],int no_allele,struct randomization sample)
  /*
int a[LENGTH];
int no_allele;
struct randomization sample;
FILE **outfile;
  */

{
  int i, j, k, l;
  char line[120];

  line[0] = '-';

  k = 1;

  //  outfile=fopen("outfile","w");

  printf ("Observed genotype frequencies: \n\n");
  
  for ( i = 0; i < no_allele; ++i ) {

    for ( j = k; j < k + 5; ++j ) 
	 line[j] = '-';

    line[j] = STR_END;
    k = j;

    printf ("%s\n", line);

    printf ("|");

    for ( j = 0; j <= i; ++j ) {
	 l = LL(i, j);
	 printf("%4d|", a[l]);
    }
    
    printf ("\n");
  }
  printf ("%s\n\n", line);
  printf ("Total number of alleles: %2d\n\n", no_allele);

  printf("Number of initial steps: %d\n", sample.step);
  printf("Number of chunks: %d\n", sample.group);
  printf("Size of each chunk: %d\n\n", sample.size); 

}

void random_choose ( int *k1, int *k2, int k )
  //int *k1, *k2, k;

{
  register int temp, i, not_find;
  //double drand48();
  int work[MAX_ALLELE];

  for ( i = 0; i < k; ++i )
    work[i] = i;

  //*k1 = int(gRandom->Uniform(1) * k);
   *k1 = int(rand()*k); 
   --k;

  for ( i = *k1; i < k; ++i )
    work[i] = i + 1;

  not_find = 1;
  
  while ( not_find ) {
    //i = int(gRandom->Uniform(1) * k);
    i = int(rand()*1);
    *k2 = work[i];
   not_find = 0;
  }
    
  if ( *k1 > *k2 ) {
    temp = *k1;
    *k1 = *k2;
    *k2 = temp;
  }

}


void select_index ( Index *indexf, int no_allele )
  /*
Index *indexf;
int no_allele;
  */

{

  int i1, i2, j1, j2;
  int k = 0;
  int l = 0;

/* generate row indices */

  random_choose ( &i1, &i2, no_allele );

  indexf->i1 = i1;
  indexf->i2 = i2;
  /* generate column indices */

  random_choose ( &j1, &j2, no_allele );

  indexf->j1 = j1;
  indexf->j2 = j2;
  /* calculate Delta = d(i1,j1) + d(i1,j2) + d(i2,j1) + d(i2,j2) */

  if ( i1 == j1 )
    ++k;

  if ( i1 == j2 )
    ++k;

  if ( i2 == j1 )
    ++k;

  if ( i2 == j2 )
    ++k;

  indexf->type = k;
  
  if ( ( i1 == j1 ) || ( i2 == j2 ) )
    ++l;

  indexf->cst = ( l == 1 ) ? pow(2.0, (double) k) : pow(2.0, - (double) k);
}

void test_switch (int a[LENGTH], Index indexf, int *switch_ind, int *switch_type,double *p1_rt, double *p2_rt)

  /*
int a[LENGTH];
Index indexf;
int *switch_ind, *switch_type; // switchability and type of switch 
double *p1_rt, *p2_rt;  //probability ratio 
*/
{
  register int k11, k22, k12, k21;

  *switch_ind = 0;

  k11 = L(indexf.i1, indexf.j1);
  k22 = L(indexf.i2, indexf.j2);
  k12 = L(indexf.i1, indexf.j2);
  k21 = L(indexf.i2, indexf.j1);

  if ( indexf.type <= 1 ) { /* type = 0, 1 */
    if ( a[k11] > 0 && a[k22] > 0 ) {
	 *switch_ind = 1;
	 *switch_type = 0; /* D-switchable */
	 *p1_rt = RATIO(a[k11], a[k12]) *  RATIO(a[k22], a[k21]) * indexf.cst;
    }
    if ( a[k12] > 0 && a[k21] > 0 ) {
	 *switch_ind += 1;
	 *switch_type = 1; /* R-switchable */
      *p2_rt = RATIO(a[k12], a[k11]) *  RATIO(a[k21], a[k22]) / indexf.cst;
    }

  } else {                  /* type = 2 */
    if ( a[k11] > 0 && a[k22] > 0 ) {
      *switch_ind = 1;
      *switch_type = 0; /* D-switchable */
	 *p1_rt = RATIO(a[k11],a[k12] + 1.0)*RATIO(a[k22],a[k12]) * indexf.cst;
    }
    if ( a[k12] > 1 ) {
      *switch_ind += 1;
      *switch_type = 1; /* R-switchable */
      *p2_rt = RATIO(a[k12],a[k11]) * RATIO(a[k12] - 1,a[k22]) / indexf.cst;
    }
    
  }

}

void do_switch (int a[LENGTH], Index indexf, int type )

  /*
int a[LENGTH];
Index indexf;
int type;
  */
{
  register int k11, k22, k12, k21;

  k11 = L(indexf.i1, indexf.j1);
  k12 = L(indexf.i1, indexf.j2);
  k21 = L(indexf.i2, indexf.j1);
  k22 = L(indexf.i2, indexf.j2);


  if ( type == 0 ) {  /* D-switch */
    --a[k11];
    --a[k22];
    ++a[k12];
    ++a[k21];
  } else {     /* R-switch */
    ++a[k11];
    ++a[k22];
    --a[k12];
    --a[k21];
  }
}


double cal_prob (int a[LENGTH],Index indexf,double ln_p_old,int *actual_switch)

  /*
int a[LENGTH];
Index indexf;
double ln_p_old;
int *actual_switch;
  */
{

  double p1_ratio, p2_ratio;
  register double ln_p_new;
  double rand_num;
  int switch_ind, type;
  //double drand48();
  //  void test_switch(), do_switch();

  *actual_switch = 0;

/* determine the switchability and direction of switch for given face */

  test_switch( a, indexf, &switch_ind, &type, &p1_ratio, &p2_ratio);
/*  printf("%d %d %d swhind=%d\n",indexf.i1, indexf.i2,indexf.i3,switch_ind);*/

  switch (switch_ind)
    {
    case 0:           /* non-switchable */

      ln_p_new = ln_p_old;  /* retain the pattern, probability unchanged */
      break;

    case 1:           /* partially-switchable */

	 if ( type == 1 )
	   p1_ratio = p2_ratio;
	 //rand_num = gRandom->Uniform();      
	 rand_num = rand();
      if ( rand_num < TRANS( p1_ratio ) ) {  /* switch w/ transition P TRANS */
           do_switch ( a, indexf, type );
           ln_p_new = ln_p_old + log (p1_ratio);  /* ln P_after-switch */
           *actual_switch = 1;
         } else                   /* remain the same w/ P = 1 - TRANS */
         ln_p_new = ln_p_old;           /* probability unchanged */
       break;     

    default:          /* fully switchable */
      //rand_num = gRandom->Uniform(); 
      rand_num = rand();
         if ( rand_num <= TRANS(p1_ratio)) {
           do_switch ( a, indexf, 0 ); /* D-switch */
           ln_p_new = ln_p_old + log (p1_ratio);  /* ln P_after-switch */
           *actual_switch = 2;
         } else if ( rand_num <= TRANS(p1_ratio) + TRANS(p2_ratio) ) {
           do_switch ( a, indexf, 1 ); /* R-switch */
           ln_p_new = ln_p_old + log (p2_ratio);
           *actual_switch = 2;
         } else
           ln_p_new = ln_p_old;
         break;    
    }

  return (ln_p_new);
}


void fisher ()

{

  register int i, j;

  // if ( check_file ( argc, argv, &infile, &outfile ) )
  //  exit ( 1 );

  //time(&t1);
  //lecture des data a partir du fichier infile
  read_data (a, &no_allele, &total, &sample) ;

  //  print_data (a, no_allele, sample);

  ln_p_observed = 0.0;

  ln_p_simulated = ln_p_observed;  

  p_mean = p_square = (double) 0.0;

  result.p_value = result.se = (double) 0.0;  /* initialization */

  result.swch_count[0] = result.swch_count[1] = result.swch_count[2] = 0;



  for ( i = 0; i < sample.step; ++i ) {  /* de-memorization for given steps */
     select_index ( &indexf, no_allele );
      

    ln_p_simulated = cal_prob(a, indexf, ln_p_simulated, &actual_switch);

    ++result.swch_count[actual_switch];
  }

  for ( i = 0; i < sample.group; ++i ) {  

    counter = 0;

    for ( j = 0; j < sample.size; ++j ) {

    select_index ( &indexf, no_allele );

    ln_p_simulated = cal_prob(a, indexf, ln_p_simulated, &actual_switch);

    if ( ln_p_simulated <= ln_p_observed )
	 ++counter;

    ++result.swch_count[actual_switch];

  }
    p_simulated = (double) counter  / sample.size;
    p_mean += p_simulated;
    p_square += p_simulated * p_simulated;

  }
  
  p_mean /= sample.group;
  result.p_value = p_mean;
  result.se = p_square / ((double) sample.group)/(sample.group - 1.0)
    - p_mean / ( sample.group - 1.0 ) * p_mean;
  result.se = sqrt ( result.se );

  total_step = sample.step + sample.group * sample.size;
  
  //printf("Randomization test P-value: %7.4g  (%7.4g) \n",result.p_value,
//result.se);
  //printf("Percentage of partial switches: %6.2f \n",result.swch_count[1] /
//total_step * 100);
  //printf("Percentage of full switches: %6.2f \n",result.swch_count[2] /
//total_step * 100);
  //printf("Percentage of all switches: %6.2f \n",(result.swch_count[1] +
//result.swch_count[2]) / total_step * 100 );

}

void test_equilibre_Hardy_Weinberg()
{
  int i,j,k,aa,b,l,c,num,a1,b1,k1=0;
 
  char fic1[30],fic2[30];
  cout<<"Give the name of the output file which contains p-value for all loci in each spatial zone :  ";
  cin>>fic1;
  cout<<"Give the name of the output file which contains p-value which are less than 0.05  :  ";
  cin>>fic2; 
  ofstream g1(fic1);
  ofstream g2(fic2);
  
  MTRand();

  for (i=0;i<nombre_UG;i++) //pour chaque UG
    {
      g1<<endl<<i<<"    ";
     k1=0;
     for (k=0;k<2*nombre_locus-1;k=k+2) //pour chaque locus
     {
       Matrice_occurence.Init(0,MAX_ALLELE,0,MAX_ALLELE);
       for (j=0;j<UG[i][2];j++) //pour chaque ind appartenant a cette UG
	{
	  num=int(UG[i][j+3]);//num est le numero de l'individu
	  aa=int(data[num][k+2]); // a est le 1er allele de l'ind j au locus k
	  b=int(data[num][k+3]); // b est le 2eme allele de l'ind j au locus k
          if (k1>0) { a1=aa-int(allele[k1-1])-1;b1=b-int(allele[k1-1])-1; }
	  else {a1=aa;b1=b;} 
	  Matrice_occurence[a1][b1]++;  
	} 
       k1++;
       //Fabrication de la matrice semi carre inferieure calculee en tenant
//compte de la symetrie des alleles a un locus donnee (A1A2=A2A1)
       for (l=0;l<MAX_ALLELE;l++)
	   for (c=0;c<l;c++)
	     Matrice_occurence[l][c]=Matrice_occurence[l][c]+Matrice_occurence[c][l];
       
       fisher();

       //constitution d'un fichier
      

       g1<<result.p_value<<"   ";
       if (result.p_value<0.05)
	 {
	   g2<<"In spatial zone : "<<i<<" for locus : "<<k/2;
	   g2<<"the P-value is : "<<result.p_value<<endl;
	 }
       //       cin>>attend;
     } //fin de pour chaque locus
    } // fin de pour chaque UG
}


void melange_individus_dans_UGcoord(vector<int> v1)
{

  int j,k,individu,new_individu;
  random_shuffle(v1.begin(), v1.end()); 

  //reconstruction du tableau data a partir du tableau data_sauve
  for (j=0;j<nombre_UG;j++)
    {
      for (k=0;k<UG[j][2];k++)
	{
	  individu=int(UGcoord[j][k+3]);
	  new_individu=v1[individu];
	  UGcoord[j][k+3]=new_individu;
	 
	}
    }


}


int main(int argc, char **argv)
{
  
  ////TApplication theApp("App", &argc, argv);


int i;
double dist;
char ouinonind,inputunit,outputunit;
Matrice_occurence.Init(0,MAX_ALLELE,0,MAX_ALLELE);



cout<<"*****************************************************";
cout<<endl<<"Programme GENBAR"<<endl;
 cout<<"*****************************************************"<<endl<<endl;

cout<<"Give the name of the data file : ";
cin >>fic_data;
cout<<"Give the number of individuals  : ";
cin>>nombre_individus;
cout<<"Give the number of loci : ";
cin>>nombre_locus;
cout<<endl;

vector<int> v1(nombre_individus, 0);
vector<int> v2(nombre_individus, 0);

Espacet.Init(0,100,0,100);//tableau des UGt indices utilise pour Voisin
data.Init(0,nombre_individus-1,0,2+nombre_locus*2);
data_sauve.Init(0,nombre_individus-1,0,2+nombre_locus*2);
data2.Init(0,nombre_individus-1,0,2+nombre_locus*2);
UGt.Init(0,2500,0,nombre_individus+3); //2500 nbre pr�visionnel d'UG - colonnes alleles inutile
//Un individu donne peut appartenir a au plus a 20 UG ?
Ind.Init(0,nombre_individus-1,0,1000);
allele.Init(0,nombre_locus-1);
double z,temp;

extraction_data(); 
cherche_minmax(); //cherche les latitudes minimales et maximales (en km)

 cout<<"unit of the geographical location  of the sampled individuals : Polar (deg) or Cartesian (km) ? Enter P ou C :";  
 cin>>inputunit;
 // if (inputunit=='P') 
 // {
 //  cout<<endl;
 //  cout<<"Unit of your output coordinate : Polar (long en rad, Pi/2-lat en rad) or Cartesian (km) ? Enter P or C :";
 // cin>>outputunit;
 // }

 if (inputunit=='C')
   {
cout<<"-----------------------------------"<<endl;
cout<<"Parameters defining Spatial Zones : "<<endl;
cout<<"-----------------------------------"<<endl;
cout<<"Your data are distributed in an area whith following characteristics : "<<endl;
cout<<"Longitudes (km) Minimale : "<<minX/1<<"  Maximale :"<<maxX/1<<endl;
cout<<"Latitudes  (km) Minimale : "<<minY/1<<"  Maximale :"<<maxY/1<<endl;
//calcul de la distance en km entre ns et eo
 cout<<" Distance Est-Ouest : "<<maxX-minX<<endl;
 cout<<" Distance Nord-Sud : "<<maxY-minY<<endl;
cout<<endl;
cout<<"Choose the distance (d, in kms) between two centers of the spatial zone : ";
cin>>dist;
dist=dist;
cout<<"Choose the value of the radius of the spatial zones (r, in kms): ";
cin>>rayon;rayon=rayon;
cout<<"Choose the minimal number of invividuals in each spatial zone (n, minimum advised of 20) : ";
cin>>effectif_min;
cout<<endl;
 polcar='c';
   }

 if (inputunit=='P')
   {
cout<<"Longitudes (deg) Minimale : "<<minX/1<<"  Maximale :"<<maxX/1<<endl;
cout<<"Latitudes  (deg) Minimale : "<<minY/1<<"  Maximale :"<<maxY/1<<endl; 
cout<<"For information : et the poles, latitude = 90, at the equator, latitude = 0"<<endl; 
//calcul de la distance en km entre ns et eo 
z=cos(M_PI*minY/180)*cos(M_PI*maxY/180); 
z=z+(sin(M_PI*minY/180)*sin(M_PI*maxY/180)); 
temp=6400*acos(z); 
cout<<" Distance maximale Nord-Sud  : "<<temp<<endl; 
z=cos(M_PI*maxY/180)*cos(M_PI*maxY/180)*cos(M_PI*(maxX-minX)/180); 
z=z+(sin(M_PI*maxY/180)*sin(M_PI*maxY/180)); 
temp=6400*acos(z);  
cout<<" Distance Est-Ouest : "<<temp<<" a la latitude "<<maxY/1<<"deg"<<endl; 
z=cos(M_PI*minY/180)*cos(M_PI*minY/180)*cos(M_PI*(maxX-minX)/180); 
z=z+(sin(M_PI*minY/180)*sin(M_PI*minY/180)); 
temp=6400*acos(z);  
cout<<" Distance Est-Ouest : "<<temp<<" a la latitude "<<minY/1<<"deg"<<endl; 
cout<<endl; cout<<"Choose the distance (d, in kms) between two centers of the spatial zone : "; 
cin>>dist; dist=dist; 
cout<<"Choose the value of the radius of the spatial zones (r, in kms): "; 
cin>>rayon;rayon=rayon; cout<<"Choose the minimal number of invividuals in each spatial zone (n, minimum advised of 20) : "; 
cin>>effectif_min; 
cout<<endl; 
   }

 /**********************************************************************


debut de la grande boucle


 ***********************************************************************/       

char significance,hardyweinberg;
  
 if (inputunit=='C') unite_geographique_cart(rayon,dist,effectif_min);
     else unite_geographique_pol(rayon,dist,effectif_min);
   // {
   // if (outputunit=='P') unite_geographique_pol(rayon,dist,effectif_min);
   //  else unite_geographique_cart(rayon,dist,effectif_min);
 // }

recodage_donnees_format_Pierre();
					   
//cout<<"Do you want to test the equilibrum of hardy weinberg (fisher method) ? (y/n) ";
// cin>>hardyweinberg;
// if (hardyweinberg=='y') test_equilibre_Hardy_Weinberg(); 
					   
 cout<<"Do you want to assess the significance of slopes ? (y/n) ";
 cin>>significance;
 if (significance=='y')
 {
	cout<<"How many random permutations do you want ? (minimum advised of 1000)";
 	cin>>nb_repetitions;
 }
 else nb_repetitions=1;
					   
for (i=0;i<nombre_individus;i++) v1[i]=i;
Statsup_Pente.Init(0,nombre_UG);
Pente_obs.Init(0,nombre_UG);
 cout<<endl<<"I am working ..... "<<endl;
for (nbr=0;nbr<nb_repetitions;nbr++)
{

  //initialisation de tableaux
proba.Init(0,nombre_UG-1,0,nombre_individus);
Homozygotes.Init(0,nombre_UG-1,0,nombre_locus-1);
Heterozygotes.Init(0,nombre_UG-1,0,nombre_locus-1);
Pente.Init(0,nombre_individus+1,0,nombre_UG); //tableau des pentes pour chaque
//individu dans ch UG, les trois dernieres lignes contiennent la moyenne et la
//variance pour tous les individus dans une UG.

Total_petits_effectifs.Init(0,nombre_UG);
 
if (nbr>0) 
melange_individus_dans_UGcoord(v1);
creation_tableau_Ind();
// cout<<"passage1"<<endl;
if (nb_repetitions > 1) 
  {
    cout<<nbr+1<<"  "<<flush;
  }
int j;

// initialisation remplissage partiel du tableau UG
 Nbgenes.Init(0,nombre_UG,0,nombre_locus);
UG.Init(0,nombre_UG,0,3+nombre_individus+nombre_alleles);
 for (i=0;i<nombre_UG+1;i++)
   for (j=0;j<3+nombre_individus;j++)
     UG[i][j]=UGcoord[i][j];
  
 // cout<<"passage2"<<endl;
calcul_frequences_alleliques_par_UG();

 

frequence_alleles.Init(0,nombre_UG-1,0,nombre_alleles);
Alleles_nuls.Init(0,nombre_UG);
Gradient_allele.Init(0,nombre_UG-1,0,nombre_alleles);
Modalite.Init(0,nombre_alleles-1);
// cout<<"passage3"<<endl;
construction_matrice_freq_all();

construit_tableau_modalite();//On calcule le nombre de modalit� par allele 
for (i=0;i<nombre_individus;i++)
{
     construction_matrice_freq_all(); 
     recalcul_frequences_alleliques(i); //on recalcule les occurences all en enlevant l'ind
      //on calcule les freq
     proba_rannala(i);

}

 if (nb_repetitions==1)
{ 
  

do
  {
  cout<<"For which invividual do you want a file of probability (-1 if finish) (called proba_i_out, where i is the individual number) ? ";
  cin>>i;
  if ((i>0) && (i<nombre_individus))
    genere_fichiers_proba_ours(i);
  }
 while ((i>0) && (i<nombre_individus));


}

   


calcul_la_pente();

int nug;
if (nbr==0) 
  for (nug=0;nug<nombre_UG;nug++)
    Pente_obs[nug]=Pente[nombre_individus][nug];

for (nug=0;nug<nombre_UG;nug++)
{
  
   if (Pente[nombre_individus][nug]==0) {Statsup_Pente[nug]--;}
   else if (Pente[nombre_individus][nug]>Pente_obs[nug])
     Statsup_Pente[nug]++;
}

} 
int nug;
for (nug=0;nug<nombre_UG;nug++)  {Statsup_Pente[nug]=Statsup_Pente[nug]/(nb_repetitions-1);}


//genere_fichier_pente();
if (inputunit=='C') genere_fichier_pente_cart();
    else genere_fichier_pente_pol();

   return 0;
}

