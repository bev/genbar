#if defined PROJET
#include "alg-lin/vecteur.h"
#include "config.h"
#include "geom/aritm.h"
#else

#endif

#include<iostream>
#include<fstream>
#include <cstdlib>

#include <ctime>

// constructeurs
Vecteur::Vecteur()
{	I=0;
	element=NULL;
}
/*
Vecteur::Vecteur(int ii2)
{
  I=0;
  this->Init(1,ii2);
}
*/
Vecteur::Vecteur(int ii1, int ii2)
{
 I=0;
 this->Init(ii1,ii2);
}

Vecteur::Vecteur(const Vecteur & V)
{
  // cout<<"Recopie.."<<endl;
  I=0;

  i1=V.i1;
  i2=V.i2;
  if(V.I>0)
 {
  this->Init(i1,i2);
  double * sourceptr = & V[i1];
  double * targetptr = & (*this)[i1];
  for (int i=i1;i<=i2;i++){
    
        // (*this)[i]=V[i];
    * targetptr = * sourceptr;
    sourceptr++;
    targetptr++;
  }
 }// if
}

Vecteur::~Vecteur()
{
  this->Libere();
}

// initialisateur-------------------------------------------
void Vecteur::Init(int ii1, int ii2,double a)
{
	this->Init(ii1,ii2);
	(*this).Remplit(a);
}
//...................
void Vecteur::Init(int ii1, int ii2)
{
  if (I>0)
    this->Libere();
  
  I=ii2-ii1+1;
  i1=ii1;
  i2=ii2;
  
  if(I>0)
    element=new double[I];
  else
    I=0;
  (*this).Remplit(0);
}

//... remplit de valeurs al�atoires entre 0 et a
void Vecteur::Aleatoire(double a)
{
srand (time (0));

for (int i=i1;i<=i2;i++)
  (*this)[i]=a*(rand()%10000)/10000.0;

}

// libere memoire------------------------------
void Vecteur::Libere()
{
  if (I>0)
    delete[] element;

  I=0;
}

//surcharges d'operateurs------------------------------------------------
double & Vecteur::operator[](int i)  const
{
#if defined(TABLEAU_TEST)
 if(I<1 ||i<i1 ||i>i2)
	{
	  cerr<<"!!erreur Vecteur hors limites!! i="<<i<<endl;
		return element[0];
	 }
 else return element[i-i1];
#else
  return element[i-i1]; 
#endif


}
//-------------------------------------------------------------
void Vecteur::operator=(double a)
{  for (int ligne=0;ligne<I;ligne++)
		element[ligne]=a;
}


//-------------------------------------------------------------
Vecteur & Vecteur::operator=(const Vecteur & v2)
{
  
 if(v2.I>0) 
      this->Init(v2.i1,v2.i2);
  else I=0;

  if ((I==v2.I)&&(I>0))
  {
    double * sourceptr = &v2[v2.i1];
    double * targetptr = & (*this)[i1];
    for (int i=i1;i<=i2;i++){
          // (*this)[i]=v2[i-i1+v2.i1];
      * targetptr = * sourceptr;
      targetptr++;
      sourceptr++;
    }
  }
  else
    cerr<<"erreur : vecteur mal dimensione pour = des vecteurs: V1="<<(*this)<<" V2="<<v2<<endl;
  return (*this);
}


 //-------------------------------------------------------------
Vecteur   Vecteur::operator+(const Vecteur & v2) const
{	Vecteur temporaire(i1,i2);
	if ((I==v2.I))
	for (int i=i1;i<=i2;i++)
			temporaire[i]=(*this)[i]+v2[i-i1+v2.i1];
	else
			  cerr<<"Erreur : vecteur non dimension�e pour +  \n";

	return temporaire;
}



//-------------------------------------------------------------
Vecteur & Vecteur::operator+=( const Vecteur & v2)
{  (*this)=(*this)+v2;
  return (*this);
}
//-------------------------------------------------------------
Vecteur   Vecteur::operator-( const Vecteur  & v2) const 
{
return ((*this)+(-v2));
}
//-------------------------------------------------------------
Vecteur & Vecteur::operator-=( const Vecteur &  v2)
{  *this=*this-v2;
  return (*this);
}
//-------------------------------------------------------------
double Vecteur::operator*( const Vecteur & v2) const 
{

  double temp=0;
  if ((i1==v2.i1)&&(i2==v2.i2)){
    double * v1ptr=&(*this)[i1];
    double * v2ptr=&v2[i1];
    for (int i=i1;i<=i2;i++){
          // temp+=(*this)[i]*v2[i];
      temp+=(*v1ptr)*(*v2ptr);
      v1ptr++;
      v2ptr++;
    }
  }
  else
    cerr<<"Erreur : vecteur non dimensionee pour *";
  
  return temp;
}
//-------------------------------------------------------------
Vecteur Vecteur::operator+() const 
{	return *this;
}
//-------------------------------------------------------------
Vecteur Vecteur::operator-() const 
{  return (-1)*(*this);
}

//-------------------------------------------------------------

Vecteur Vecteur::operator*(double a) const 
{
  Vecteur temporaire(i1,i2);
  double * sourceptr=&(* this)[i1];
  double * targetptr=&temporaire[i1];
  for (int i=i1;i<=i2;i++){
//    temporaire[i]=(*this)[i]*a;
    * targetptr= (* sourceptr)*a ;
    sourceptr++;
    targetptr++;
  }
  
  return temporaire;
}
//-------------------------------------------------------------
Vecteur operator*(double a, const Vecteur & v2)
{  	return (v2*a);
}
//-------------------------------------------------------------
Vecteur operator/(double a, const Vecteur &  v2)  // U=a/V  (a* inverse des elements)
{
Vecteur temp(v2.i1,v2.i2);
for(int i=v2.i1;i<=v2.i2;i++)
  {
   temp[i]=a/v2[i];
  }
return temp;
}


//-------------------------------------------------------------
Vecteur Vecteur::operator/(double a) const 
{
  return ((*this)*(1/a));
}

//================================================
void Vecteur::Remplit(double a)  // remplit le vecteur de  a
{
  double * temp = & (*this)[i1];
  for(int i=i1; i<=i2;i++){  
        // (*this)[i]=a;
    * temp=a;
    temp++;
  }
  
}



/*=================================================
  Calcule la norme
  */
double Vecteur::Norme()
{
  return sqrt((*this).Norme2());
}
/*=================================================
  Calcule la norme   carr�
  */
double Vecteur::Norme2()
{
  return ((*this)*(*this));
}
/*=========================================================
  Norme le vecteur
*/
void Vecteur::Normer()
{
  double n=(this->Norme());
  if(n!=0)
    (*this)=(*this)/n;
  else
    cerr<<" impossible de normaliser un vecteur de norme nulle !";
}

//====== renvoit la valeur absolue des elements ===============
Vecteur  Vecteur::Abs()
{
  Vecteur temp=(*this);
  for(int i=i1;i<=i2;i++)
    if(temp[i]<0) temp[i]=-temp[i];

  return temp;
}
//=======================================================
/* trouve l'ordre croissant des elements
  sortie : vecteur : rang[i1..i2]   tel que rang[i1]= indice du premier element

  Algo : permutations par voisins
*/
 Vecteur Vecteur::Ordre_croissant()
{

  double temp;
  int tempi;

  Vecteur rang(i1,i2);
  Vecteur V(*this);

  for (int i=i1;i<=i2; i++)  // initialise
		rang[i]=i;

  for (int i=i1; i<i2;i++)
  for (int j=i2;j>i;j--)
	  {
		if(V[j]<V[j-1])
			{  // permutte
			  temp=V[j];
			  V[j]=V[j-1];
			  V[j-1]=temp;

			  tempi=(int)rang[j];
			  rang[j]=rang[j-1];
			  rang[j-1]=tempi;
			}
	  }
 return rang;
}
//============================================================
/* sortie : vecteur class�  */
void  Vecteur::Range_croissant()
{

 double temp;


  for (int i=i1; i<=i2;i++)
  for (int j=i2;j>i;j--)
	  {
		if((*this)[j]<(*this)[j-1])
			{  // permutte
			  temp=(*this)[j];
			  (*this)[j]=(*this)[j-1];
			  (*this)[j-1]=temp;
			}
	  }

}
/*========================================================
 renvoit indice  du premier max trouve
 */
int  Vecteur::Recherche_max()
{
 int im=i1;
 double max=(*this)[im];

 for (int i=i1;i<=i2;i++)
   if((*this)[i]>max) 
    {max=(*this)[i]; im=i;}
 return im;
}

/*========================================================
 recherche si la valeur val est dans le vecteur
 sortie: indice mini du vecteur  (-1001 si non trouve)
 */
int  Vecteur::Recherche(double val)
{
 int i=i1-1;
 do
     i++;
 while(( (*this)[i]!=val)&&(i<i2));
 if ( (*this)[i]!=val) return (-1001);
 else return i;
  
}

//============================================
// trouve le premier indice i tel que val< V[i+1]
// renvoit i   ( i2 si V[i2]<val )
int  Vecteur::Recherche_interval(double val)
{
 int i=i1;

 while(((*this)[i]<val)&&(i<i2))
   i++;

return i-1;
}

/*===================================
 Permutte les elements
  vecteur de permuttation: i2=rang[i1]; element i2 va en  i1.
  */
void Vecteur::Permutte(Vecteur & rang)
{
Vecteur temp=(*this);

for (int i=i1;i<=i2;i++) 
	 (*this)[i]=temp[(int)rang[i]];
}
/*===================================
 Permutte circulaire de (d) elements
  */
void Vecteur::Permut_circ(int d)
{


#if defined PROJET
Vecteur temp=(*this);

for (int i=i1;i<=i2;i++) 
	 (*this)[i]=temp[ireste((i-d-i1),I)+i1];
#endif
}
/*=====================================
  Ajuste deux vecteurs V et W a valeur dans ]-Pi,Pi[,
 cad trouve la rotation qui minimise la somme des ecarts au carre.
 entree: V (this), W
 sortie: decalage d tel que,  V[i1 (premier element)]=W[i1+d] 
*/ 
int Vecteur::Ajuste(Vecteur  W)
{
  if(I!=W.I) cerr<<" erreur: vecteurs de taille differentes dans Ajuste"<<endl;

  double r;
  double rmin=((*this)-W).Norme2();
  int dmin=0;
  
  for(int d=1;d<I;d++)  // decalage
    {
      W.Permut_circ(d);  
      if((r=((*this)-W).Norme2())<rmin)  
	    {dmin=d;
             rmin=r;
            }
    }

  return dmin;
}



/*=====================================================
 Cree un tableau * double
  et y copie le vecteur
	dans les places i=1,...,I
						 j=1,...,I
  ( � d�sallou� avec Tab1_del
*/
double * Vecteur::Tab1()
{
double * elem;
	int i,ni;
	ni=I;

	//      elem=dvector(1,I); //alloue mem

	elem=new double [ni+1];

	for (i=1; i<=ni;i++)
		  elem[i]=element[i-1];

	return elem;
}

//-------------------------------------------------
int Vecteur::Tab1_del(double * elem)
{
  	delete[] elem;

  //  free_dvector(elem,1,I);

  return 0;
}
/* ------------------------------------------
 Copie un tableau *double vers Vecteur
 */
int Vecteur::Tab1_copie(double * elem)
{
  int i,ni;
  ni=I;
  double * temp= element;
  for (i=1; i<=ni;i++){
//    element[i-1]=elem[i];
    elem++;
    * temp = * elem;
    temp++;
  }
  
  return 0;
}

//------------------------------------
// pour afficher le contenu du vecteur
ostream & operator <<(ostream & sortie,  const Vecteur & vect)
{
if (vect.I>0)
	{
	sortie<<"Vecteur ["<<vect.i1<<".."<<vect.i2<<"] "<<endl<<"|";
	for (int i=vect.i1;i<=vect.i2;i++)
	{ sortie<<vect[i]<<"|";
	}
	sortie<<"\n";
	}
return sortie;
}

//=======================================
/*---ecrit le contenu du vecteur dans un fichier --
format : I , i1, i2, contenu

*/


void Vecteur::Ecrit_fichier(ofstream & fich)
{
  fich<<I<<endl<<i1<<endl<<i2<<endl;

 for(int i=i1;i<=i2;i++)
   fich<<(*this)[i]<<endl;

}

//=======================================
void Vecteur::Ecrit_fichier(char * nom_fich)
{
 cout<<" Ecrit Vecteur dans fichier: "<<nom_fich<<endl;
 ofstream fich(nom_fich);
 Ecrit_fichier(fich);
}
//=================================================
/*---lit le contenu du vecteur depuis un fichier --
format : I , i1, i2 , contenu
*/

void Vecteur::Lit_fichier(ifstream & fich)
{
  int II,ii1,ii2;
  fich>>II>>ii1>>ii2;

  Init(ii1,ii2);

  for(int i=i1;i<=i2;i++)
    fich>>(*this)[i];

}
//====================================
void Vecteur::Lit_fichier(char * nom_fich)
{
  ifstream fich(nom_fich);
  Lit_fichier(fich);
}
//=========================== GRAPHISME ============================
#if defined ROOT
#include <TCanvas.h>
#include <TGraph.h>
#include <TPolyMarker.h>
#include <TEllipse.h>
#include <TH1.h>

//========= dessin ========================
TCanvas*  Vecteur::Dessin(char * titre)
{
return Dessin("i","V[i]",i1,i2,titre);
}
//========= dessin ========================
TCanvas*  Vecteur::Dessin(char * xtitre,char * ytitre,double xmin,double xmax,char * titre)
{
TCanvas *c = new TCanvas(titre,titre,2);
Dessin(xtitre, ytitre,xmin, xmax, titre, c,0);
return c;
}

//========= dessin ========================
//  c : fenetre ou dessiner  
// superp: =0 si premier figure, 1,2.. si superposition (style de ligne)
TCanvas*  Vecteur::Dessin(char * xtitre,char * ytitre,double xmin,double xmax,char * titre,TCanvas * c,int superp,char* opt)
{
 int nx(I);
 TH1F *h1=new TH1F(titre,titre,nx,xmin,xmax);

 h1->SetXTitle(xtitre);
 h1->SetYTitle(ytitre);
 for(int  i=0;i<nx;i++)  // remplissage 
      {
       double z=(*this)[i+i1];
        h1->SetBinContent(i+1,z);
      }
 //---- definit le marker P ----
 h1->SetMarkerColor(1); 
 h1->SetMarkerStyle(6); // 3:etoile  6: petit cercle
 h1->SetLineStyle(1); // 1=solid, 2=dash, 3=dash-dot, 4=dot-dot
 h1->SetLineColor(1);


if(superp==0)
  h1->Draw(opt); // L: ligne  P: marker
else 
  {
  h1->SetLineStyle(superp); // 1=solid, 2=dash, 3=dash-dot, 4=dot-dot
  h1->Draw("sameL"); // L: ligne entre  P: marker
  }
 c->Update();
return c;
}
//========= dessin (x[i],y[i] = objet)========================   
TCanvas*  Vecteur::Dessin(Vecteur &x,char * xtitre,char * ytitre,char * titre)
{

 if(x.I !=I) {cerr<<"Il faut x.I=I dans Vecteur.cc"<<endl; return 0;}

 TCanvas *c = new TCanvas(titre,titre,2);
 int nx(I);


 float *xx=new float[nx];
 float *yy=new float[nx];
  
   for (int i=0;i<nx;i++) 
   {
     xx[i] =x[i+x.i1];
     yy[i] =(*this)[i+i1];
   }

 
   TGraph  *gr= new  TGraph(nx,xx,yy);

   /*
 (gr->GetXaxis())->SetLabel(xtitre);
 (gr->GetYaxis())->SetLabel(ytitre);
 */

  gr->Draw("AL*");
  
  c->Update();

return c;
}
//========= dessin sur cercle========================
void  Vecteur::Dessin_phases(char * titre)
{
  TCanvas *c = new TCanvas(titre,titre,2);
 int nx(I);


 float *x=new float[nx+2];
 float *y=new float[nx+2];
  
   for (int i=0;i<nx;i++) 
   {
     x[i] = cos((*this)[i+i1]);
     y[i] = sin((*this)[i+i1]);
   }
   x[nx]=1; y[nx]=1; // pour cadrer
   x[nx+1]=-1; y[nx+1]=-1;

   TGraph  *gr= new  TGraph(nx+2,x,y);
 
   gr->Draw("A*");
    TEllipse *e=new  TEllipse(0,0,1);
   e->Draw();
    
  c->Update();

}


//========= dessin sur colonne a l abcisse x========================
void  Vecteur::Dessin_col(double xx, TCanvas *c)
{
     c->cd();
     float *x=new float[I];
     float *y=new float[I];

     for (int i=0;i<I;i++) 
       {
	 x[i] = xx;
	 y[i] = (*this)[i+i1];
       }
     


     TPolyMarker *pp= new TPolyMarker(I,x,y,"*");
     pp->SetMarkerColor(1); 
     pp->SetMarkerStyle(6); // petit cercle
     pp->Draw();
     

     /*
    TGraph  *gr= new  TGraph(I,x,y);
    gr->Draw("sameAC*");
    */
     c->Update();
}

#endif // graphisme
