// version du 5 janvier 99
/* version du 21 mai 1996
    22 decembre 97
    */
 
#if !defined(__MATRICE_H)
#define __MATRICE_H


#if defined PROJET
#include "alg-lin/vecteur.h"
#else
#include "vecteur.h"
#endif


class Matrice
{	public:

//membres--------------------------------------
	int i1,i2; // numero de la premiere et derniere ligne
	int j1,j2; // numero de la premiere et derniere colonne
	int I;  // nombre de lignes
	int J;  // nombre colonnes
        Vecteur * ligne;   // tableau de ligne
	 
         //double * elements;   // rangement de clapack:  a(i,j) =  A[j*n+i] = elements[(j-j1)*I+(i-i1)]

//constructeurs-------------------------------
	Matrice();     // matrice vide
	Matrice(int ii1,int ii2,int jj1,int jj2,double a=0);    // matrice remplie de 0 indices:i1->i2
	Matrice(int ii2,int jj2,double a=0);    // matrice remplie de 0 indices:i1->i2
//	Matrice(int ii2,double a=0); // matrice carr�e
	Matrice(const Matrice & M);

	~Matrice();

 
// initialisateurs-------------------------------------
	void Init(int ii1,int ii2,int jj1,int jj2);   // matrice carr�e remplie de 0


// libere memoire------------------------------
	void Libere();

//... remplit de valeurs al�atoires entre 0 et a
	void Aleatoire(double a);

//operateurs-------------------------------------
	Vecteur & operator[](int i) const;       // renvoit la ligne i

  	void operator=(double a);          // remplit chaque element= a
	 Matrice & operator=(const Matrice & m2);  // recopie

	Matrice operator+(const Matrice & m2) const; // A=B+C
	void operator+=(const Matrice & m2); // A+=B
	Matrice operator-(const Matrice &  m2) const;  // A=B-C
	void operator-=(const Matrice & m2);          // A-=B
	Matrice operator*(const Matrice & m2) const;     // A=B*C

	friend Matrice operator*(double a,const Matrice & m2);  // A=a*B
	Matrice operator*(double a) const;  //A=B*a
	Matrice operator/(double a) const;  // A=B/a

	Vecteur operator*(const Vecteur & v) const;  // A=B*v
        
        Matrice operator+(double  a) const;         // A=A+a      (identite)
        friend Matrice operator+(double a,const Matrice & m2); //A=a+B 
	Matrice operator-(double  a) const;          // A=B-a 
	friend Matrice operator-(double a,const Matrice & m2); // A=a-B

//===============================================
  // operateurs unaires
Matrice operator+() const;
Matrice operator-() const;
//===============================================
friend Matrice Diag(const Vecteur & v);  // renvoit une matrice diagonale
void Remplit(double a);  // remplit la matrice de  a
Matrice Transpose();
Matrice Inverse();  // par methode gauss jordan
/*=========================================================
 Extrait la colonne j d'une matrice
  sortie : vect[*] = M[*][j]
 */
Vecteur  Extrait_col(int j);

/*=========================================================
Copie un vecteur dans la colonne j d'une matrice
  sortie :  M[*][j]= vect[*]
 */
void Copie_col(const Vecteur & vect,int j);

/*=================================================================
Normalisation des colonnes
*/
int Normer_colonne();

//=============================================================
 /* diago une matrice sym  IxI
	 entree :  matrice, remplie triangle inf  , dimension val_p[a,b] tel que b-a=I
	sortie : matrice vect_p[c,i] = vecteur propre n�i, pour i=a->b
							 composantes c=i1->i2
			 val_p[i] = val_p n�i      i=a->b

  */

Matrice Diagosym(Vecteur & val_p);
//=============================================================
/* diago une matrice reelle  quelconque par Clapack

    (il y a aussi dgeevx.C pour expert..)
*/
Matrice Diago(Vecteur & val_pr,Vecteur & val_pi );

/*=====================================================
 Cree un tableau ** double
  et y copie la matrice  CARREE
	dans les places i=1,...,taille
						 j=1,...,taille
  ( � d�sallou� avec Tab2_del
*/
double ** Tab2();
int Tab2_del(double ** element);
/*
 Copie un tableau **double vers Matrice
 */
int Tab2_copie(double ** element);

// resoud AX=B : nr p48--------------------------------------------
// sortie : X   et determinant de A: det
	Vecteur Resoud1_AXB(Vecteur & B,double & det);
/* resoud AX=B par la m�thode Singular decomposition Value
  NR p64
	sortie : X
*/
  Vecteur Resoud2_AXB(Vecteur & B);
//------------------------------------
// pour afficher le contenu de la matrice
friend std::ostream & operator <<(std::ostream & sortie, const Matrice & M);

/*---ecrit le contenu de la matrice dans un fichier --
format : I ,J, i1, i2 ,j1,j2,  contenu
*/
void Ecrit_fichier(char const * nom_fich=TEMP"/mat.txt");
void Ecrit_fichier_matrice(std::ofstream & fich);
void Lit_fichier(char const * nom_fich=TEMP"/mat.txt");
void Lit_fichier(std::ifstream & fich);

//========= dessin ========================


#if defined ROOT
TCanvas *  Matrice::Dessin(char * titre="Matrice");
TCanvas *  Matrice::Dessin(char * xtitre,double xmin,double xmax,char * ytitre,double ymin,double ymax,char * titre,char * option="surf1");
//  c : fenetre ou dessiner  
// superp: =0 si premier figure, 1,2.. si superposition
TCanvas*  Matrice::Dessin(char * xtitre,double xmin,double xmax,char * ytitre,double ymin,double ymax,char * titre,char * option,TCanvas * c,int superp);
#endif
};
#endif

