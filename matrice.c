

#if defined PROJET  // si on travail en projet ============
#include "alg-lin/matrice.h"
#include "geom/aritm.h"
#include "recipes/nr-perso.h"  // numerical Recipes
#include "config.h"
#else  // si pas de projet ========================
#define mini(A,B) ((A)<(B)?(A):(B))
#endif


#include <cstdio>
#include <cstdlib>
#include <ctime>

#include <iostream>
#include <fstream>

// constructeurs------------------------------------------------------
Matrice::Matrice()
{
	I=0;
        ligne=NULL;
	//elements=NULL;
}
/*
Matrice::Matrice(int ii2,double a) // matrice carr�e
{
 I=0;
 this->Init(1,ii2,1,ii2);
 for (int i=1;i<=mini(I,J);i++)
			(*this)[i+i1-1][i+j1-1]=a;
}
*/
Matrice::Matrice(int ii2,int jj2,double a)
{
 I=0;
 this->Init(1,ii2,1,jj2);
for (int i=1;i<=mini(I,J);i++)
			(*this)[i+i1-1][i+j1-1]=a;
}

Matrice::Matrice(int ii1,int ii2,int jj1,int jj2,double a)
{
 I=0;
 this->Init(ii1,ii2,jj1,jj2);
 for (int i=1;i<=mini(I,J);i++)
			(*this)[i+i1-1][i+j1-1]=a;
}

Matrice::Matrice(const Matrice & M)
{
 I=0;
 J=0;

 i1=M.i1;
 i2=M.i2;
 j1=M.j1;
 j2=M.j2;
 this->Init(i1,i2,j1,j2);
 for (int i=i1;i<=i2;i++)
  for (int j=j1;j<=j2;j++)
			(*this)[i][j]=M[i][j];
}

Matrice::~Matrice()
{
this->Libere();
}

// initialisateurs -----------------------------------------------
void Matrice::Init(int ii1,int ii2,int jj1,int jj2)
{  if ((I*J)>0)  // d�truit le contenu existant
	  this->Libere();

	i1=ii1; i2=ii2; j1=jj1; j2=jj2;
	I=i2-i1+1;
	J=j2-j1+1;

	//if ((I*J)>0)  elements=new double[I*J];
		
        if (I>0)
	{
		ligne=new Vecteur[I];
		for (int i=0;i<I;i++)
			ligne[i].Init(j1,j2);
	}
	
}
// libere memoire------------------------------
void Matrice::Libere()
{
if (I>0)
	{	for (int i=1;i<=I;i++)
		  ligne[i-1].Libere();
	 	delete[] ligne;   
	}

//if ((I*J)>0) 	delete[] elements;    
	
 I=0;
 J=0;
}

//========================================================
//... remplit de valeurs al�atoires entre 0 et a
void Matrice::Aleatoire(double a)
{
 srand (time (0));
 for(int i=i1;i<=i2;i++)
    for(int j=j1;j<=j2;j++) 
        (*this)[i][j]=a*(rand()%10000)/10000;

}
//===============================================================
Vecteur &  Matrice::operator[](int i) const
{
#if defined(TABLEAU_TEST)
 if(I<1 || i<i1 ||i>i2)
	{
	  std::cerr<<"!!erreur Matrice hors limites!! i="<<i<<std::endl;
	return ligne[0];
	 }
  else
        return ligne[i-i1];
 // return  elements[(j-j1)*I+(i-i1)];
#else
    return ligne[i-i1]; 
    // return  elements[(j-j1)*I+(i-i1)];
#endif
}
//=================================================

void Matrice::operator=(double a)
{
	for (int i=i1;i<=i2;i++)
	  for (int j=j1;j<=j2;j++)
             (*this)[i][j]=a;		
}
//--------------------------------------------------
Matrice & Matrice::operator=(const Matrice & m2)
{ 

 this->Init(m2.i1,m2.i2,m2.j1,m2.j2);
 

 if ((I==m2.I)&&(J==m2.J))
	{	for (int i=i1;i<=i2;i++)
			for (int j=j1;j<=j2;j++)
				(*this)[i][j]=m2[i-i1+m2.i1][j-j1+m2.j1];
	}
	
  return (*this);
}

//===============================================
Matrice Matrice::operator+( const Matrice & m2) const 
{
	Matrice tempo(i1,i2,j1,j2);  // cree une matrice identique


	if ((I==m2.I)&&(J==m2.J))     // matrices de m�me taille
		for (int i=i1;i<=i2;i++)
			for (int j=j1;j<=j2;j++)
				tempo[i][j]=(*this)[i][j]+m2[i-i1+m2.i1][j-j1+m2.j1];
	else
		std::cerr<<"Erreur : matrice non dimension�e pour +  \n";


	return tempo;
}
//===============================================
void Matrice::operator+=( const Matrice & m2)
{  (*this)=(*this)+m2;
}
 //===============================================
Matrice Matrice::operator-( const Matrice & m2) const 
{
 return ((*this)+(-m2));
}
//===============================================
void Matrice::operator-=( const Matrice & m2)
{  (*this)=(*this)-m2;
}
 //===============================================
Matrice Matrice::operator*( const Matrice & m2) const 
{

	Matrice tempo(i1,i2,m2.j1,m2.j2);  // cree une matrice resultat


	if ((j1==m2.i1)&&(j2==m2.i2))     // condition pour multiplication
		for (int i=i1;i<=i2;i++)
			for (int j=m2.j1;j<=m2.j2;j++)
			  {
				tempo[i][j]=0;
				for(int k=j1;k<=j2;k++)
					tempo[i][j]+=(*this)[i][k]*m2[k][j];
			  }


	else
		std::cerr<<"Erreur : matrice non dimensionee pour *";

	return tempo;

}


//===============================================
Matrice Matrice::operator*(double a) const 
{
	Matrice  tempo(i1,i2,j1,j2);  // cree une matrice resultat

	for (int i=i1;i<=i2;i++)
			for (int j=j1;j<=j2;j++)
			 tempo[i][j]=a*(*this)[i][j];

	return tempo;

}
//===============================================
Matrice operator*(double a, const Matrice & m2)
{
	return (m2*a);
}
//===============================================
Matrice Matrice::operator/(double a) const 
{
 return ((*this)*(1/a));
}
//===============================================
Vecteur Matrice::operator*( const Vecteur & v) const 
{
	Vecteur tempo(i1,i2);
	if ((j1==v.i1)&&(j2==v.i2))
		for(int i=i1;i<=i2;i++)
		{  tempo[i]=0;
			for(int j=j1;j<=j2;j++)
				tempo[i]+=(*this)[i][j]*v[j];
		}
	else
		std::cerr<<"Erreur : matrice-vecteur non dimension�e pour *";

	return tempo;
}
//===============================================
Matrice Diag( const Vecteur & v)
{	Matrice m(v.i1,v.i2,v.i1,v.i2);
	for (int i=v.i1;i<=v.i2;i++)
		m[i][i]=v[i];
	return m;
}
//===============================================
  // operateurs unaires
Matrice Matrice::operator+() const 
{ 	return (*this);
}
//===============================================
Matrice Matrice::operator-() const 
{	return ((-1)*(*this));
}

//================================================
void Matrice::Remplit(double a)  // remplit la matrice de  a
{
  for(int i=i1; i<=i2;i++)
	for(int j=j1; j<=j2;j++)
	  (*this)[i][j]=a;

}
//===============================================
Matrice Matrice::Transpose()
{  Matrice temp(j1,j2,i1,i2);

	for(int i=i1;i<=i2;i++)
		for(int j=j1;j<=j2;j++)
			temp[j][i]=(*this)[i][j];

	return temp;
}
//===============================================
Matrice Matrice::Inverse()  // par methode gauss jordan
{
if (I!=J)
		std::cerr<<" erreur : matrice non carree ";

//... copie matrices dans tableaux...
		double ** TM;
		TM=(*this).Tab2();
		

// calcul de inverse par numerical recipes
		double **b;
#if defined PROJET 
	gaussj(TM,I,b,0);
#endif

// copie resultat 
   Matrice Inv(i1,i2,j1,j2);
    Inv.Tab2_copie(TM);

//...libere mem..
	(*this).Tab2_del(TM);

return Inv;
}
//=============================================
/* diagonalisation avec numerical recipes
  ( Clapck serait  dsyevdx.C )
*/
Matrice  Matrice::Diagosym(Vecteur & val_p)
{

	if (I!=J)
		std::cerr<<" erreur : matrice non carree ";

	if((val_p.I!=I))
	  std::cerr<<" erreur :  val mal dimensionn�es";



	//... copie matrices dans tableaux...
		double ** TM;
		TM=(*this).Tab2();
		double *TV1;
		TV1=val_p.Tab1();        // d
		double *TV2;
		TV2=val_p.Tab1();        // e


	// calcul de diago par numerical recipes
#if defined PROJET
	tred2(TM,I,TV1,TV2);
	tqli(TV1,TV2,I,TM);
#endif
	// copie resultat  dans objets temporaires (non class�s)
	Vecteur val_pt(val_p.i1,val_p.i2);
	Matrice vect_pt(j1,j2,val_p.i1,val_p.i2);

	val_pt.Tab1_copie(TV1);
	vect_pt.Tab2_copie(TM);

	//...classe les valeurs propres

	Vecteur rang=val_pt.Ordre_croissant();

	Matrice vect_p(j1,j2,val_p.i1,val_p.i2);

	for (int i=val_p.i1;i<=val_p.i2;i++)   // numero de la val_p
	{  val_p[i]=val_pt[(int)rang[i]];
		for (int j=j1;j<=j2;j++)      // composantes
			vect_p[j][i]=vect_pt[j][(int)rang[i]];

	}

	//...libere mem..
	(*this).Tab2_del(TM);
	val_pt.Tab1_del(TV1);
	val_pt.Tab1_del(TV2);

	//  normalisation des Vecteurs propres colonnes
	vect_p.Normer_colonne();

	return vect_p;
}
//=============================================================
 /* diago une matrice reelle  quelconque par Clapack

    (il y a aussi dgeevx.C pour expert..)
  */
#if defined PROJET
#include "f2c_perso.h"  // Clapack

extern "C"  dgeev_(char *jobvl, char *jobvr, integer *n, doublereal * a,
           integer *lda, doublereal *wr, doublereal *wi, doublereal *vl,
           integer *ldvl, doublereal *vr, integer *ldvr, doublereal *work,
           integer *lwork, integer *info);


#endif
//-------------------------------------------------
Matrice Matrice::Diago(Vecteur & val_pr,Vecteur & val_pi )
{
#if defined PROJET
  if (I!=J)
    cerr<<" erreur : matrice non carree dans Diago I="<<I<<" J="<<J<<endl;
  
  if((val_pr.I!=I))
    cerr<<" erreur :  val p. mal dimensionn�es: val_p.I="<<val_pr.I<<" et I="<<I<<endl; 
 

  Matrice vect_p(j1,j2,val_pr.i1,val_pr.i2);
 

  //----- variables pour Clapack
char *jobvl="N";  
char *jobvr="V";  // on veut les vecteurs propres a droite
integer n=I;     // taille matrice (>=0)
integer lda=n;
doublereal *a;  // matrice  lda*n

doublereal *wr;  // val propres en sortie (partie reelle)
doublereal *wi;  //(partie imaginaire)

doublereal *vl;  // non utilise
integer ldvl=1;


doublereal *vr;// vect p. en sortie (en colonnes)
integer ldvr=n;

doublereal *work;  // dim lwork
integer lwork=4*n;
 integer info;  // info en sortie 0: OK


a= new doublereal[lda*n];
wr= new doublereal[n]; 
wi= new doublereal[n]; 

vl= new doublereal[ldvl*n];
vr= new doublereal[ldvr*n];

work= new doublereal[lwork];
   
  //-- on remplit matrice a ----
for(int i=i1;i<=i2;i++)
 for(int j=j1;j<=j2;j++)
        a[(j-j1)*I+(i-i1)]=(*this)[i][j];
      

//---- appel de Clapack

 dgeev_(jobvl,jobvr,&n,a,&lda,wr,wi,vl,&ldvl,vr,&ldvr,work,&lwork,&info);


if(info!=0)
  cerr<<"erreur dans Diago (matrice.cc): info="<<info<<". Lire Doc de CLapack sur la routine dgeev"<<endl;

//--- on recopie ---------------

for(int e=val_pr.i1; e<=val_pr.i2; e++)   // niveaux
  {
   val_pr[e]=wr[e-val_pr.i1];
   val_pi[e]=wi[e-val_pi.i1];
   for(int i=i1; i<=i2; i++)  // composantes
      vect_p[i][e]=vr[(e-val_pr.i1)*I+(i-i1)];
   
  }
                   
//---- on libere tableaux ----                    
delete [] a; 
delete [] wr;
delete [] wi;
delete [] vl;
delete [] vr;
delete [] work;

return vect_p;


#endif
}

/*=========================================================
 Extrait la colonne j d'une matrice
  sortie : vect[*] = M[*][j]
 */
Vecteur  Matrice::Extrait_col(int j)
{
	Vecteur vect(i1,i2);
	if ((j<=j2)&&(j>=j1))
		for (int i=i1;i<=i2;i++)
			vect[i]=(*this)[i][j];
	else std::cerr<<" !! erreur extrait col \n";

 return vect;
}
/*=========================================================
Copie un vecteur dans la colonne j d'une matrice
  sortie :  M[*][j]= vect[*]
 */
void Matrice::Copie_col( const Vecteur & vect,int j)
{
	if((vect.i1!=i1)||(vect.i2!=i2))
	    std::cerr<<" !! erreur : vecteur mal dimensione ";
	else
	  {
	  if ((j>=j1)&&(j<=j2))
		for (int i=i1;i<=i2;i++)
			(*this)[i][j]=vect[i];
		else std::cerr<<" !! erreur : j hors dimension matrice";
	  }

}
/*=================================================================
Normalisation des colonnes
*/
int Matrice::Normer_colonne()
{
	for (int j=j1;j<=j2;j++)
	{
		Vecteur vect_col=(*this).Extrait_col(j);
		vect_col.Normer();
		this->Copie_col(vect_col,j);
	}

	return 0;
}



/*=====================================================
 Cree un tableau ** double
  et y copie la matrice
	dans les places i=1,...,I
						 j=1,...,J
  ( � d�sallou� avec Tab2_del
*/
double ** Matrice::Tab2()
{
	double ** element;
	int i,j;

        
	//     element=dmatrix(1,I,1,J); //alloue mem

	
	element=new double *[I+1];
	for (i=0;i<=I;i++)
		element[i]=new double[J+1];
	

	for (i=1; i<=I;i++)
		for (j=1;j<=J;j++)
		  element[i][j]=(*this)[i-1+i1][j-1+j1];

	return element;
}
//=======================================================
int Matrice::Tab2_del(double ** element)
{
  
	for (int i=0;i<=I;i++)
			delete[] element[i];
		delete[] element;
   

		//free_matrix(element,1,I,1,J);

  return 0;
}
/*=======================================================
 Copie un tableau **double vers Matrice
 */
int Matrice::Tab2_copie(double ** element)
{
	int i,j;

	for (i=1; i<=I;i++)
		for (j=1;j<=J;j++)
		  (*this)[i-1+i1][j-1+j1]=element[i][j];

  return 0;
}
//=============================================================

Vecteur Matrice::Resoud1_AXB(Vecteur & B,double & det)
{
//... cf numerical recipies p48
if(I!=J)
	std::cerr<<("erreur : matrice non carree");

if(((B.I)!=I)||(B.i1!=i1)||(B.i2!=i2))
	std::cerr<<("erreur : vecteur de mauvaise taille");


//.... copie matrices dans tableaux

int *indx;
indx=new int[I+1];

//... copie matrices dans tableaux...

double ** TM;
TM=this->Tab2();

double *TB;
TB=B.Tab1();

#if defined PROJET
ludcmp(TM,I,indx,&det);
lubksb(TM,I,indx,TB);
#endif

// copie resultat

Vecteur X(j1,j2);


X.Tab1_copie(TB);

// cf numerical recipes p49
for(int j=1;j<=I;j++) det*=TM[j][j];



//...libere mem..
this->Tab2_del(TM);
B.Tab1_del(TB);

delete[] indx;

return X;
}

//==============================================
/* resoud AX=B par la m�thode Singular decomposition Value
  NR p64
*/

Vecteur Matrice::Resoud2_AXB(Vecteur & B)
{
//... cf numerical recipies p64

if(((B.I)!=I)||(B.i1!=i1)||(B.i2!=i2))
	std::cerr<<("erreur : vecteur de mauvaise taille");

//... objets temporaires..
Vecteur W(1,I);
Matrice V(1,I);

//... reserve place...
double ** TM;
TM=this->Tab2();
double ** TV;
TV=V.Tab2();

double* TW;
TW=W.Tab1();

//.. singular value decomp de  TM -> TW,TV
#if defined PROJET
svdcmp(TM,I,I,TW,TV);
#endif

//.... recherche de la val propre max en valeur abs
double max=fabs(TW[1]);
int j;
for ( j=1;j<=I;j++)
  if (fabs(TW[j])>max)   max=fabs(TW[j]);
//... on met a zero les petites valeurs propres
for ( j=1;j<=I;j++)
  if (fabs(TW[j])<max*1e-5)   TW[j]=0;


// Resultat ...
Vecteur X(j1,j2);

double* TX;
TX=X.Tab1();

double* TB;
TB=B.Tab1();

//...substitue -> TX
#if defined PROJET
svbksb(TM,TW,TV,I,I,TB,TX);
#endif
//...copie
X.Tab1_copie(TX);

//...libere mem..
this->Tab2_del(TM);
V.Tab2_del(TV);
W.Tab1_del(TW);
B.Tab1_del(TB);
X.Tab1_del(TX);


return X;
}

//------------------------------------
// pour afficher le contenu de la matrice
std::ostream & operator <<(std::ostream & sortie, const  Matrice & M)
{
if (M.I>0)
	{
	sortie<<"Matrice ["<<M.i1<<".."<<M.i2<<";"<<M.j1<<".."<<M.j2<<"] \n |";
	for (int i=M.i1;i<=M.i2;i++)
	  {
		for  (int j=M.j1;j<=M.j2;j++)
		  sortie<<M[i][j]<<"\t|";
		 
		std::cout<<"\n |";
	  }

	sortie<<"\n";
	}
return sortie;
}
/*---ecrit le contenu de la matrice dans un fichier --
format : I ,J, i1, i2 ,j1,j2,  contenu
*/
//=======================================
void Ecrit_fichier_matrice(std::ofstream & fich)
{
fich<<I<<std::endl<<J<<std::endl<<i1<<std::endl<<i2<<std::endl<<j1<<std::endl<<j2<<std::endl;
for(int j=j2;j>=j1;j--)
    for(int i=i1;i<=i2;i++)
        fich<<((*this)[i][j])<<std::endl;
}
//=======================================
void Ecrit_fichier(char * nom_fich)
{
    std::cout<<" Ecrit matrice dans fichier: "<<nom_fich<<std::endl;
    std::ofstream fich { nom_fich };
    Ecrit_fichier_matrice(fich);
}
//=======================================

//----------------------------------------
void  Matrice::Lit_fichier(char * nom_fich)
{
 ifstream fich(nom_fich);
 Lit_fichier(fich);
}
//----------------------------------------
void Lit_fichier(std::ifstream & fich)
{
int II,ii1,ii2,JJ,jj1,jj2;
fich>>II>>JJ>>ii1>>ii2>>jj1>>jj2;

Init(ii1,ii2,jj1,jj2);

for(int j=j2;j>=j1;j--)
  for(int i=i1;i<=i2;i++)
    fich>>(*this)[i][j];
}
//=========================== GRAPHISME ============================
#if defined ROOT
#include <TStyle.h>
#include <TCanvas.h>
#include <TH2.h>

//========= dessin ========================
TCanvas*  Matrice::Dessin(char * titre)
{
return Dessin("i",i1,i2,"j",j1,j2,titre,"cont");
}
//========= dessin ========================
TCanvas*  Matrice::Dessin(char * xtitre,double xmin,double xmax,char * ytitre,double ymin,double ymax,char * titre,char * option)
{
TCanvas *c = new TCanvas(titre,titre,2);


Dessin(xtitre,xmin,xmax,ytitre,ymin,ymax,titre,option,c,0); 

return c;
}
//========= dessin ========================
//  c : fenetre ou dessiner  
// superp: =0 si premiere figure, 1 si superposition
TCanvas*  Matrice::Dessin(char * xtitre,double xmin,double xmax,char * ytitre,double ymin,double ymax,char * titre,char * option,TCanvas * c,int superp)
{ //-------
char options_2[30];
strcpy(options_2,option);
if(superp!=0)
   strcat(options_2,"same");

//-----------------------

 int nx(I),ny(J);



 TH2F *h=new TH2F(titre,titre,nx,xmin,xmax,ny,ymin,ymax);

 h->SetXTitle(xtitre);
 h->SetYTitle(ytitre);

 for( int i=i1;i<=i2;i++)  // remplissage 
   for(int j=j1;j<=j2;j++)
     {
     double z=(*this)[i][j];
     h->SetCellContent(i-i1+1,j-j1+1,z);
     }
  //-----palette de gris -------pour:TH2F -SetContour(7); Draw("cont" ou "surf2");------
  Int_t colors[]={1,0,19,18,17,15,13,1,1}; // (ordre:des indices dans la palette 4,5,6,7,0,1,3)
   gStyle->SetPalette(8,colors); // classe TStyle
   //-----------
  

 h->SetContour(7);
 
 h->SetEntries(1); // pour voir les lignes avec "cont".



 c->Clear(); // efface les dessins sous-jacents

h->Draw(options_2);

 c->Update();
return c;
}

#endif // graphisme

