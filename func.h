/***************************************************************************


  func.h 


  header file containing the function names.


  status: OK


  date: 1/15/90


***************************************************************************/


//char *ctime();


//double rans();
//double log_factorial();
double ln_p_value(int a[LENGTH],int no_allele,double constant);
double cal_prob(int a[LENGTH],Index index,double ln_p_old,int *actual_switch);
//double cal_const();


//void read_data(int a[LENGTH],int *no_allele,int  *total, struct randomization *sample, FILE **infile  );
void print_data(int a[LENGTH],int no_allele,struct randomization sample,FILE **outfile);
//void get_interval();
void select_index(Index *index,int no_allele);
//void cal_n();
//void stamp_time();
