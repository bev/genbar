//Nouveau code pour les AFLP seulemement.
//J'enlève tout ce qui est superflu et je garde que ce dont j'ai besoin 
//donnes en km ou en degré
//les donnes AFLp doivent être codées 0 ou 1
//donnees manquantes negatives
// au max 1000 UG (fichier rind)
//Le numeros des ind dans UG vont de 0 à nbre_UG et donc il faut donc ajouter 1 pour le num de l'ind
//dans Ind -->les numeros d'UG sont corrects
//Je considère que si la frequence des 1 est connue celle des 0 peut se retrouver!

//void affiche_UG_numero(int num) A FAIRE




#include <iostream.h>
#include <fstream.h>
#include <strstream>
#include <vector>
#include "MersenneTwister.h"
#include "vecteur.h" //classe cree par frederic pour les tableaux
#include "matrice.h"  //classe cree par frederic pour les tableaux
#include "matrice.cc"
#include "vecteur.cc"
#include <algorithm>

#include "hwe.h"
#include "func.h"

using namespace std;

Matrice data;
Matrice UG;
Matrice UGt;
Matrice UGcoord;
Matrice Voisin; 
Matrice Poids;
Matrice Poids_total;
Matrice Espace(0,200,0,200); //Pourquoi initialisée maintenant? Pourquoi plus grand que Espace?
Matrice Espacet;
Matrice Ind;
Matrice Nbgenes;//contient pour chaque UG le nombre total d'individus par locus
Matrice frequence_alleles;
Matrice proba;
Matrice Pente;

Vecteur Corres;
Vecteur Statsup_Pente;
Vecteur Pente_obs;

int nombre_individus,nombre_locus;
int effectif_min;
int nombre_UG;
int nbr;
int Nombre_min_voisin;
int nb_repetitions;

char fic_data[100];
double minX,maxX,minY,maxY;
double rayon;
double minpente,maxpente;

//fichier de sortie pour le test
ofstream g1("sortie");

void extraction_data()
//extrait les positions et les valeurs des loci 
{
int i,j;
char nom_ind[10];
char reponse;
cout<< endl;
ifstream f(fic_data);
ofstream g("noms_individus");
cout<<"Est ce que vos donnees contiennent un champ identificateur de l'individu ? (o/n) : ";
 cin>>reponse;    
for (i=0;i<nombre_individus;i++)

   {  
     if (reponse=='o') f>>nom_ind;
        g<<i<<"   "<<nom_ind<<endl;
     for (j=0;j<2+nombre_locus;j++)
       {
	 f>>data[i][j];
       }
   }
}

void cherche_minmax ()
//retourne les valeurs minX,maxX,minY et maxY correspondant aux valeurs min
//et max des deux premieres colonnes du tableau tab (ie latitude, longitude)
{

int i=0;

minX=data[0][0];
maxX=minX;
minY=data[0][1];
maxY=minY;
for (i=1;i<nombre_individus;i++)
{
if (data[i][0]<minX)
   minX=data[i][0];
else
   if (data[i][0]>maxX) maxX=data[i][0];
}
for (i=1;i<nombre_individus;i++)
{
if (data[i][1]<minY) minY=data[i][1];
        else
   if (data[i][1]>maxY) maxY=data[i][1];
   }
cout<<"minX="<<minX<<" maxX="<<maxX<<" minY="<<minY<<" maxY="<<maxY<<endl;
}

double distancekm (int u1, int u2) //renvoie la distance en km entre les deux UGs
//u1 et u2 du tableau UG -- mais attention UG est indicee de 0 a nombre_UG-1 alors
//que u1 et u2 vont de 1 a nombre_UG

{
  double temp,z,L1,l1,L2,l2;
  L1=UGcoord[u1-1][0]; //longitude = X 
  l1=UGcoord[u1-1][1]; // latitude = Y
  L2=UGcoord[u2-1][0];
  l2=UGcoord[u2-1][1];
  z=sin(l2)*sin(l1)*cos(L1-L2);
  z=z+(cos(l1)*cos(l2));

  temp=6400*acos(z);
  //cout<<"distance entre les UG "<<u1-1<<" et "<<u2-1<<" : "<<temp<<" km"<<endl;
   return temp;
  }



double distancekm_cart (int u1, int u2) //renvoie la distance en km entre les
//deux UGs u1 et u2 du tableau UG -- mais attention UG est indicee de 0 a
//nombre_UG-1 alors que u1 et u2 vont de 1 a nombre_UG
{
  double temp,z,x,xp,y,yp;
  x=UGcoord[u1-1][0]; //longitude = X 
  y=UGcoord[u1-1][1]; // latitude = Y
  xp=UGcoord[u2-1][0];
  yp=UGcoord[u2-1][1];
  
  z=(y-yp)*(y-yp);
  z=sqrt((x-xp)*(x-xp)+z);

  temp=z;
  return temp;
}






void unite_geographique_cart (double R,double d,int effmin)  //effmin: effectif
//min dans le cercle
//Met en place la grille, ainsi que les unites geographiques (UG) (cercle de
//rayon R)
//La grille est d'unite la distance d des UG

//on attribue a chaque individu ses UG dans le tableau individu
//X est la coordonnee selon l'axe 0 (<=> longitude), Y la coordonnees selon l'axe
//1 (<=> latitude)
{
int i=0,j=0,k=0,ind=0;
int temp1=0; //indice l'unite geographique
int temp2=0;
int nbre_div=0;
double X=0,Y=0,z=0,total=0;
char fin='N';
//Vecteur N;
//Vecteur M;


cherche_minmax();

//Les tableaux  sont initialises dans le main

X=minX;
Y=minY;
i=0;j=0;

do //Pour chaque unite geographique
{
        temp1=i+j*nbre_div;
        UGt[temp1][0]=X;
        UGt[temp1][1]=Y;
        for (k=0;k<nombre_individus;k++) //Pour chaque individu
        {
	  z=(data[k][0]-X)*(data[k][0]-X);
	  z=z+(data[k][1]-Y)*(data[k][1]-Y);
	  
        if (z<=(R*R))
          {
            UGt[temp1][2]= UGt[temp1][2]+1; //Nombre d'individus appartenant au
//cercle de rayon R
            temp2= 2+int(UGt[temp1][2]);
            UGt[temp1][temp2]= k; //On inscrit l'individu dans le tableau UG
          }
        }
        if (X<maxX)
// tant que X est plus petit que Xmax, on l'incremente - l'increment tient compte
//de
// la latitude du centre de l'UG precedente
        {
    
          Espacet[i][j]=temp1+1; //on ajoute 1 pour ne pas confondre le 0 de
//l'init et le 0 de l'UG 0	  
          i=i+1;
          X=X+d;
	 
        }
        else
//on incremente Y ie la latitude et on reinitialise la longitude a la valeur min
        {
          j=j+1;
          nbre_div=i+1;
          i=0;
          Y=Y+d;
          X=minX;

        }

//somme total des individus sur les UG (doit etre sup a nombre_individus
total=total+UGt[temp1][2];

if (Y>maxY && X>maxX) fin='O';
}
while (fin=='N');

//construction du tableau UGcoord dont chaque UG contient au moins effmin individus 
//c'est la différence avec UGt

ind=0; 
for (i=0;i<temp1;i++)
   if (UGt[i][2]>=effmin)    
     ind++;//nombre UG avec plus de effmin individus
     
nombre_UG=ind;
cout<<"With your data and parameters, the number of spatial zone is : "<<nombre_UG<<endl;

UGcoord.Init(0,nombre_UG,0,3+nombre_individus); // idem UG mais sans colonnes alleles???En fait ce qui change c'est initialisation colonne
Corres.Init(0,2500);
ind=0;
for (i=0;i<temp1;i++) // avant c'etait temp-1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   if (UGt[i][2]>=effmin)   
     {     
       Corres[i+1]=ind+1;//ind n'est incrémenté que si eff>=effmin
       UGcoord[ind]=UGt[i];
       ind++;
     }

//On construit le tableau espace à partir de espacet et de Corres pour ne retenir
//que les UG qui contiennent effmin individus

 int temp4;//temp4 n'est pas initialisé? Est-ce normal?
 for (i=0;i<100;i++)
   {
   for (j=0;j<100;j++)
     {
       temp4=int(Espacet[i][j]);
       Espace[i][j]=Corres[temp4];
     }
   }

Voisin.Init(1,nombre_UG,0,8);
Poids_total.Init(1,nombre_UG,0,8);
Poids.Init(1,nombre_UG,0,8); //contient pour chaque UG la distance (en km) entre
//le centre de l'UG considérée et chacun de ses huit voisins.
//remplissage du tableau voisin et du tableau Poids
//Cas general et cas particuliers (bords). Attention le tableau voisin est indicé
//de 1 à 92!!!


 for (i=0;i<100;i++) //espace est initialisé de 0 à 100
   for (j=0;j<100;j++)
     if (Espace[i][j]>0)
     { 
       temp4=int(Espace[i][j]);
       if (j>0)
	 {
	   if (Espace[i][j-1] >0) 
	     {
       	       Poids[temp4][0]=1/distancekm_cart(temp4,int(Espace[i][j-1]));
	      
	       Voisin[temp4][8]++;
	     }
	   Voisin[temp4][0]=Espace[i][j-1];
	 }

       if ((j>0) && (i<100-1))
	 {
	   if (Espace[i+1][j-1] >0) 
	     {
	       Poids[temp4][1]=1/distancekm_cart(temp4,int(Espace[i+1][j-1]));
	      
	       Voisin[temp4][8]++;
	     }
	   Voisin[temp4][1]=Espace[i+1][j-1];
	 }
       
       if ((i<100-1) && (j<100-1))
	 {
	   if (Espace[i+1][j+1] >0) 
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][2]=1/distancekm_cart(temp4,int(Espace[i+1][j+1]));
	      
	     }
	   Voisin[temp4][2]=Espace[i+1][j+1];
	 }

       if (i<100-1)
	 {
	   if (Espace[i+1][j] >0) 
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][3]=1/distancekm_cart(temp4,int(Espace[i+1][j]));
	     }
	   Voisin[temp4][3]=Espace[i+1][j];
	 }

       if ((i>0) && (j<100-1))
	 {
	   if (Espace[i-1][j+1] >0) 
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][4]=1/distancekm_cart(temp4,int(Espace[i-1][j+1]));
	     }
	   Voisin[temp4][4]=Espace[i-1][j+1];
	 }

       if (i>0)
	 {
	   //	   if (Espace[i-1][j]-1 >0) pourquoi ????
	   if (Espace[i-1][j] >0)
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][5]=1/distancekm_cart(temp4,int(Espace[i-1][j]));
	     }
	   Voisin[temp4][5]=Espace[i-1][j];
	 }

       if ((i>0) && (j>0))
	 {
	   //	   if (Espace[i-1][j-1]-1 >0) pourquoi ???
	   if (Espace[i-1][j-1] >0)
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][6]=1/distancekm_cart(temp4,int(Espace[i-1][j-1]));
	     }
	   Voisin[temp4][6]=Espace[i-1][j-1];
	 }

       if (j<100-1)
	 {
	   //	   if (Espace[i][j+1]-1 >0) pourquoi ???
	   if (Espace[i][j+1] >0)
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][7]=1/distancekm_cart(temp4,int(Espace[i][j+1]));
	     }
	   Voisin[temp4][7]=Espace[i][j+1];
	 }
     }




}



void unite_geographique_pol (double R,double d,int effmin)  //effmin: effectif
//min dans le cercle
//Met en place la grille, ainsi que les unites geographiques (UG) (cercle de
//rayon R)
//La grille est d'unite la distance d des UG

//on attribue a chaque individu ses UG dans le tableau individu
//X est la longitude, Y la lattitude
{
int i=0,j=0,k,ind;
int temp1; //indice l'unite geographique
int temp2;
int nbre_div=0;
double X,Y,z,cosR,total,temp;
char fin='N';

Vecteur M;

 for (i=0;i<nombre_individus;i++)
   {
//transformation des coord exprimees en degre, en radian
data[i][0]=(M_PI/180)*data[i][0];
data[i][1]=(M_PI/180)*data[i][1];
//transformation de la latitude (deplacement de l'origine)
data[i][1]=(M_PI/2)-data[i][1];
//cout<<"long"<<data[i][0]<<"   et lat  "<<(M_PI/2)-data[i][1]<<endl;
   }
//on intervertit minY et maxY parce que le changement de coordonnees (deg en rad)
//rend le min max et vice versa

temp=minY;minY=maxY;maxY=temp;
minX=(M_PI/180)*minX;
minY=(M_PI/2)-(M_PI/180)*minY;
maxX=(M_PI/180)*maxX;
maxY=(M_PI/2)-(M_PI/180)*maxY;
 
i=0;
X=minX;
Y=minY;
cosR=cos(R/6400);

do //Pour chaque unite geographique
{
        temp1=i+j*nbre_div;
        UGt[temp1][0]=X;
        UGt[temp1][1]=Y;
        for (k=0;k<nombre_individus;k++) //Pour chaque individu
        {
	  z=sin(Y)*sin(data[k][1])*cos(data[k][0]-X);
	  z=z+(cos(data[k][1])*cos(Y));
	  //z=cos(Y)*cos(data[k][1])*cos(data[k][0]-X);
	  //z=z+(sin(data[k][1])*sin(Y));

        if (z>=cosR)
          {
            UGt[temp1][2]= UGt[temp1][2]+1; //Nombre d'individus appartenant au
//cercle de rayon R
            temp2= 2+int(UGt[temp1][2]);
            UGt[temp1][temp2]= k; //On inscrit l'individu dans le tableau UG
          }
        }
        if (X<maxX)
// tant que X est plus petit que Xmax, on l'incremente - l'increment tient compte
//de
// la latitude du centre de l'UG precedente
        {
    
          Espacet[i][j]=temp1+1; //on ajoute 1 pour ne pas confondre le 0 de
//l'init et le 0 de l'UG 0	  
          i=i+1;
          X=X+(d/6400)*(1/sin(Y));
	  //	  cout <<"UG(X)  "<<X<<endl;
        }
        else
//on incremente Y ie la latitude et on reinitialise la longitude a la valeur min
        {
          j=j+1;
          nbre_div=i+1;
          i=0;
          Y=Y+(d/6400);
          X=minX;
	  
        }

//somme des individus appartenant a ch UG (doit etre sup a nombre_individus
total=total+UGt[temp1][2];

if (Y>maxY && X>maxX) fin='O';
}
while (fin=='N');

 
//construction du tableau UG dont chaque UG contient au moins effmin individus
 
ind=0; 
for (i=0;i<temp1;i++)
   if (UGt[i][2]>=effmin)    
     ind++;
     
nombre_UG=ind;
cout<<"nombre UG = "<<nombre_UG<<endl;

//UG.Init(0,nombre_UG,0,3+nombre_individus+500);
UGcoord.Init(0,nombre_UG,0,3+nombre_individus);
Corres.Init(0,2500);
 char attend;
 ind=0;
for (i=0;i<temp1-1;i++)
   if (UGt[i][2]>=effmin)   
     { 
       Corres[i+1]=ind+1;
       UGcoord[ind]=UGt[i];
       ind++;
     }

//cout<<"appuyer sur une touche pour continuer ";
//cin>>attend;
//On construit le tableau espace à partir de espacet et de Corres ppur ne retenir
//que les UG qui contiennent effmin individus

 int temp4;
 for (i=0;i<100;i++)
   {
   for (j=0;j<100;j++)
     {
       temp4=int(Espacet[i][j]);
       Espace[i][j]=Corres[temp4];
   
     }
   }
Voisin.Init(1,nombre_UG,0,8);
Poids_total.Init(1,nombre_UG,0,8);
Poids.Init(1,nombre_UG,0,8); 

//contient pour chaque UG la distance (en km) entre
//le centre de l'UG considérée et chacun de ses huit voisins.
//remplissage du tableau voisin et du tableau Poids
//Cas general et cas particuliers (bords). Attention le tableau voisin est indicé
//de 1 à 92!!!

 for (i=0;i<100;i++)
   for (j=0;j<100;j++)
     if (Espace[i][j]>0)
     { 
       temp4=int(Espace[i][j]);
       if (j>0)
	 {
	   if (Espace[i][j-1] >0) 
	     {
     	       Poids[temp4][0]=1/distancekm(temp4,int(Espace[i][j-1]));
	       Voisin[temp4][8]++;
	     }
	   Voisin[temp4][0]=Espace[i][j-1];
	 }

       if ((j>0) && (i<100-1))
	 {
	   if (Espace[i+1][j-1] >0) 
	     {
	      
	       Poids[temp4][1]=1/distancekm(temp4,int(Espace[i+1][j-1]));
	       Voisin[temp4][8]++;
	     }
	   Voisin[temp4][1]=Espace[i+1][j-1];
	 }
       
       if ((i<100-1) && (j<100-1))
	 {
	   if (Espace[i+1][j+1] >0) 
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][2]=1/distancekm(temp4,int(Espace[i+1][j+1]));
	     }
	   Voisin[temp4][2]=Espace[i+1][j+1];
	 }

       if (i<100-1)
	 {
	   if (Espace[i+1][j] >0) 
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][3]=1/distancekm(temp4,int(Espace[i+1][j]));
	     }
	   Voisin[temp4][3]=Espace[i+1][j];
	 }

       if ((i>0) && (j<100-1))
	 {
	   if (Espace[i-1][j+1] >0) 
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][4]=1/distancekm(temp4,int(Espace[i-1][j+1]));
	     }
	   Voisin[temp4][4]=Espace[i-1][j+1];
	 }

       if (i>0)
	 {
	   //	   if (Espace[i-1][j]-1 >0) pourquoi ????
	   if (Espace[i-1][j] >0)
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][5]=1/distancekm(temp4,int(Espace[i-1][j]));
	     }
	   Voisin[temp4][5]=Espace[i-1][j];
	 }

       if ((i>0) && (j>0))
	 {
	   //	   if (Espace[i-1][j-1]-1 >0) pourquoi ???
	   if (Espace[i-1][j-1] >0)
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][6]=1/distancekm(temp4,int(Espace[i-1][j-1]));
	     }
	   Voisin[temp4][6]=Espace[i-1][j-1];
	 }

       if (j<100-1)
	 {
	   //	   if (Espace[i][j+1]-1 >0) pourquoi ???
	   if (Espace[i][j+1] >0)
	     {
	       Voisin[temp4][8]++;
	       Poids[temp4][7]=1/distancekm(temp4,int(Espace[i][j+1]));
	     }
	   Voisin[temp4][7]=Espace[i][j+1];
	 }
     }
 

}

void creation_tableau_Ind()
{
Vecteur N;
int i,j,k,No;
//on cree le tableau inverse Individus qui contiendra les references
//des UG auxquelles il appartient

N.Init(0,nombre_individus);
//pour chaque UG

for (k=0;k<nombre_UG;k++)
  if (UGcoord[k][2]!=0)
        {
  for (j=0;j<UGcoord[k][2];j++) //pour chaque individu inscrit dans cette UG
    {
    i=int(UGcoord[k][3+j]);//i est donc la ref de l'individu contenu dans cette UG
    N[i]=N[i]+1;//N est un compteur pour chaque ind du nombre d'UG auquelles il
//appartient
    No=int(N[i]);
    Ind[i][No]=k;
    
    }

  }
//Pas très clair pour moi comment marche la procédure. En particulier le nbr
//Est-ce qu'on doit donner une valeur a nbr? Procedure appelée dans recodage Pierre
//et enfin on sait a combien d'UG appartient l'individu
UGcoord[nombre_UG][2]=nombre_individus;
for (i=0;i<nombre_individus;i++)
{
  Ind[i][0]=N[i];
  if (N[i]==0)
    { 
      if (nbr==0) cout<<"Pay attention : Individual "<<i<<" is present in any Spatial Zone "<<endl<<flush;
      UGcoord[nombre_UG][2]--;
    }
}

g1<<"Fichier ind   "<<endl;
for (int k=0;k<nombre_individus;k++)
	{
		g1<<k+1<<",";
		for (int j=0;j<nombre_UG;j++)
    			{
     			 g1<<Ind[k][j]<<" , ";
			}
     		 g1<<endl;
	}
}


void calcul_frequence_bandesAFLP_par_UG()
{
  int i=0,j=0,k=0,l=0,m=0;
  int a=0;

  //Initialisation des colonnes UG
 for (i=0;i<=nombre_UG;i++)

    for (j=nombre_individus+3;j<3+nombre_individus+nombre_locus+1;j++)
      UG[i][j]=0;


 for (i=0;i<nombre_UG;i++)//  pour toutes les UG avec eff min
   {
     for (j=0;j<UG[i][2];j++)//pour chaque individu de cet UG
       {
	 k=int(UG[i][j+3]);//k est l'indice de l'individu dans les tab data
	 for (l=0;l<nombre_locus;l++)
	 {
	   m=int(data[k][l+2]);//code de l'allele au locus l de l'individu k
		if (m==1)
	   {
	   UG[i][nombre_individus+3+l]++;//si la bande est presente on incrémente 
	   //sinon il peut y avoir un 0, ou un -9 (donnee manquante)
	   //on regarde si l'individu dans l'UG i à une bande au locus considéré
	   //si oui, on compte nombre de bande dans chaque UG par locus
	   
	   }
	 }

	}
	       int ind;
	       for (l=0;l<nombre_locus;l++)//pour chaque locus
	          for(k=0;k<UG[i][2];k++)//pour chaque ind
	            { 
		      ind=int(UG[i][3+k]);
		      m=int(data[ind][2+l]);
			    if (m>=0)     //test sur la valeur manquante
			    Nbgenes[i][l]++;//Nombre de 0 ou de 1 a chaque loci dans chaque UG-valeurs manquantes
			    }
	}

 //calcul du nombre de presence de bandes par UG, range dans UG a la colonne nbre_individus+3+nombre_locus

	       for (i=0;i<nombre_UG;i++)
	         {
	           for (j=nombre_individus+3;j<nombre_individus+3+nombre_locus;j++)
		   UG[i][nombre_individus+3+nombre_locus]=UG[i][nombre_individus+3+nombre_locus]+UG[i][j];
		 }
	       for (i=0;i<nombre_individus;i++)
	       for (j=0;j<nombre_locus;j++)
 //calcul du nombre total des presence de bande pour chaque locus
 //on range cette donnee dans le tableau UG a la ligne nombre_UG
	 {
	   a=int(data[i][j+2]); //code de l'allele au locus j de l'individu i
	   if (a==1)
		{
	   UG[nombre_UG][nombre_individus+3+j]=UG[nombre_UG][nombre_individus+3+j]+1;
		}
	 }

g1<<"Fichier Nbgenes  "<<endl;
for (int k=0;k<nombre_UG;k++)
	{
		g1<<k<<",";
		for (int j=0;j<nombre_locus;j++)
    			{
     			 g1<<Nbgenes[k][j]<<" , ";
			}
     		 g1<<endl;
	}
}


 void affiche_totaux()
{
  int i,j;
cout<<"Occurences des bandes dans l'ensemble de la population :"<<endl;
  for (j=nombre_individus+3;j<nombre_individus+3+nombre_locus;j++)
      cout<<"locus : "<<j-nombre_individus-3<<"   occurence :"<<UG[nombre_UG][j]<<endl;
  cout<<endl;
  cout<<"Nombre total de bandes par UG :"<<endl;
  for (i=0;i<nombre_UG;i++)
    cout <<"UG : "<<i<<" nombre total :"<<UG[i][nombre_individus+3+nombre_locus]<<"  soit "<<UG[i][nombre_individus+3+nombre_locus]/(nombre_locus)<<" individus"<<endl;
}

void construction_matrice_freq_all()//range les frequences dans un tableau

{
  int i,j;
for (i=0;i<nombre_UG;i++)
    for (j=0;j<nombre_locus;j++)
	frequence_alleles[i][j]=0;

 
  for (i=0;i<nombre_UG;i++)
    {
    for (j=0;j<nombre_locus;j++)
      {
      frequence_alleles[i][j]=UG[i][j+3+nombre_individus];
  
      }
    }
g1<<"Frequence allele  "<<endl;
for (int k=0;k<nombre_UG;k++)
	{
		g1<<k<<",";
		for (int j=0;j<nombre_locus;j++)
    			{
     			 g1<<frequence_alleles[k][j]<<" , ";
			}
     		 g1<<endl;
	}
}

//void affiche_UG_numero(int num)



void recalcul_frequences_bandes(int num_ind)
//recalcule des frequences de bandes en enlevant l'individu des UG auquelles il
  //appartient.
{
  int k=0,l=0,ug=0;
  double m=0;
  for (k=0;k<Ind[num_ind][0];k++) //pour toutes les UG auquelles appartient cet
//individu
    {
    for (l=0;l<nombre_locus;l++) //pour tous les genes de cet individu
      {
        m=data[num_ind][l+2]; //m est le code de l'allele, m est égal à -1, 0 ou 1
      ug=int(Ind[num_ind][k+1]); //ug est le numero de l

       if (m>0) 
	{
	frequence_alleles[ug][l]--; //on retire l'allele de l'ind du nombre
	g1<<"j'ai trouve "<<"  num ind"<<k<<"  num UG  "<<ug<<endl;
}


      }
    }

g1<<"Frequence allele modifie "<<endl;
for (int k=0;k<nombre_UG;k++)
	{
		g1<<k<<",";
		for (int j=0;j<nombre_locus;j++)
    			{
     			 g1<<frequence_alleles[k][j]<<" , ";
			}
     		 g1<<endl;
	}

}

void calcul_proba_genotype_par_ours(int i)
//calcul de la probabilite theorique du genotype de l'ours i pour chaque
//UG selon la méthode de Paetkau
  //affiche la vraisemblance (-log p)
{
  int j,k,kp,l;
  double m;
  
  for (j=0;j<nombre_UG;j++)
  {
  proba[j][i]=1;
  for (k=0;k<nombre_locus;k++) //pour chaque locus de l'individu
      {      
        m=data[i][k+2];//valeur du locus : 1, 0 ou -1
        
            for (l=0;l<Ind[i][0];l++) //pour toutes les UG auxquelles appartient
//cet ind
             // if (Ind[i][l+1]==j) //si l'ind appartient a l'UG
		if (Ind[i][l+1]==j)

//l+1 car dans la première colonne on a le nombre d'UG auxquel appartient l'individu. Bizarre
//dans le code francoise Ind[i][l]???
              {
//cout<<"freq_allele  "<<frequence_alleles[j][k]<<endl;
		//Si l'individu a la bande et que l'allele est présent dans l'UG
             	if (m==1 && frequence_alleles[j][k]!=0)
 		proba[j][i]=proba[j][i]*frequence_alleles[j][k]/Nbgenes[j][k];
        	//l'individu a la bande, mais l'allele n'est pas la
		else if (m==1 && frequence_alleles[j][k]==0)
	  	proba[j][i]=proba[j][i]/UG[j][2];
		//l'individu n' a pas de bande et fréquence non nulle de pas de bandes dans l'UG
		else if (m==0 && (Nbgenes[j][k]-frequence_alleles[j][k])!=0)
		proba[j][i]=proba[j][i]*(Nbgenes[j][k]-frequence_alleles[j][k])/Nbgenes[j][k];
		//l'individu n'a pas de bande et fréquence nulle de pas de bandes dans l'UG
		else if (m==0 && (Nbgenes[j][k]-frequence_alleles[j][k])==0)
		proba[j][i]=proba[j][i]/UG[j][2];


		else if (m!=-1)
	cout<<"problem for individual "<<i<<" at marker "<<k<<endl;
		}
	}
  }

}

/*
void genere_fichiers_proba_ours(int i)
  //genere autant de fichiers que d'individus - le nom du fichier est le nom de l'individu
{ 
  
  int k;
  char nom_fic[20];
  cout<<"Give a filename for the individual "<<i<<" probability file : ";
  //cin>>fichier;
  cin>>nom_fic;
  // ofstream g(fichier);
  ofstream g(nom_fic);
  for (k=0;k<nombre_UG;k++)
    {
    g<<k<<" , "<<UG[k][0]<<" , "<<UG[k][1]<<", "<<-log10(proba[k][i])<<endl;
    }
}



void genere_un_seul_fichier_proba_ours()
  //genere un fichier de format : num_ug,x,y,proba_ours1,proba_ours2,...
  // procédure non utilisée - pour polaire
{
  int k,i;
  ofstream g("probas_ours.txt");
  for (k=0;k<nombre_UG;k++)
    {
      g<<k<<" , "<<UG[k][0]*(180/M_PI)<<" , "<<(M_PI/2 - UG[k][1])*(180/M_PI)<<",";
      for (i=0;i<nombre_individus;i++)
	g<<-log10(proba[k][i])<<" , ";
      g<<endl;
    }
}
*/


void genere_un_seul_fichier_proba()
  //genere un fichier de format : num_ug,x,y,proba_ours1,proba_ours2,...
  // procédure non utilisée - 
{
  int k,i;
  ofstream g("probas.txt");
  for (k=0;k<nombre_UG;k++)
    {
      g<<k<<" , "<<UG[k][0]<<" , "<<UG[k][1]<<",";
      for (i=0;i<nombre_individus;i++)
	g<<-log10(proba[k][i])<<" , ";
      g<<endl;
    }

}


void calcul_la_pente()
  //calcule la pente moyenne par UG pour tous les individus
  //les valeurs pour les voisins sont pondérées par la distance entre les voisins
{
int i=0,j=0,k=0;
double temp=0;
double totalpoids=0;
Nombre_min_voisin=6;
  for (i=1;i<nombre_UG;i++)
      for (k=0;k<8;k++)
	Poids_total[i][k]=Poids[i][k];

k=0;
for (i=0;i<nombre_individus;i++)
  for (j=0;j<nombre_UG-1;j++)
{
  if (Voisin[j+1][8]>=Nombre_min_voisin)
    {
      for (k=0;k<8;k++)
	{
	  if (Voisin[j+1][k]!=0)  //si ce voisin existe
	    {
	      // 
temp=temp+Poids_total[j+1][k]*fabs(-log10(proba[j][i])+log10(proba[(Voisin[j+1][k]-1)][i]));
	     
temp=temp+fabs(-log10(proba[j][i])+log10(proba[(Voisin[j+1][k]-1)][i]));
	      totalpoids=totalpoids+Poids_total[j+1][k];
	    }
	}
      Pente[i][j]=temp/Voisin[j+1][8];//(totalpoids);
      temp=0;totalpoids=0;
    }
  }

     for (j=0;j<nombre_UG-1;j++)
       {
        for (i=0;i<nombre_individus;i++) 
	  Pente[nombre_individus][j]=Pente[nombre_individus][j]+ Pente[i][j];
  
	Pente[nombre_individus][j]=Pente[nombre_individus][j]/nombre_individus;
       }

//calcul de la variance sur la pente

double ecart,ecart2,sommeecarts;

      for (j=0;j<nombre_UG-1;j++)
	{
	  sommeecarts=0;
	 for (i=0;i<nombre_individus;i++)
	   { 
	     ecart=Pente[nombre_individus][j]-Pente[i][j];
	     ecart2=ecart*ecart;
	     sommeecarts=sommeecarts+ecart2;
	   }
	 Pente[nombre_individus+1][j]=sommeecarts/(nombre_individus-1);
	 Pente[nombre_individus+1][j]=sqrt(Pente[nombre_individus+1][j]);
    
       }

//recherche des valeurs min et max de la pente
     minpente=1000;maxpente=0;
     for (i=0;i<nombre_UG;i++)
       if (Pente[nombre_individus][i]<minpente)
minpente=Pente[nombre_individus][i];
       else if (Pente[nombre_individus][i]>maxpente)
maxpente=Pente[nombre_individus][i];

}  


void genere_fichier_pente_cart()
{
//Ecriture du fichier résultats / pente
//colonne 1 : longitude
//colonne 2 : latitude
//colonne 3 : pentes
//colonne 4 : pcalc

int i;
char nom_fic[20];

cout<<"Enter a name for the slopes file : ";
cin>>nom_fic;
ofstream g(nom_fic);

for (i=0;i<nombre_UG;i++)
     {
       g<<UG[i][0]<<"    "<<UG[i][1]<<"    "<<Pente_obs[i]<<"   ";
       if (Statsup_Pente[i]<0) Statsup_Pente[i]=-1; 
       if (nb_repetitions>1) g<<Statsup_Pente[i]<<"   ";
       g<<endl;
     }                               

}
void genere_fichier_pente_pol()
  //tranformation des coordonne en degre et pi/2-latitude
{
//Ecriture du fichier résultats / pente
//colonne 1 : longitude
//colonne 2 : latitude
//colonne 3 : pentes
//colonne 4 : pcalc

int i;
char nom_fic[20];

cout<<"Enter a name for the slopes file : ";
cin>>nom_fic;
ofstream g(nom_fic);

for (i=0;i<nombre_UG;i++)
     {
       g<<UG[i][0]*180/M_PI<<"    "<<(M_PI/2-UG[i][1])*180/M_PI<<"    "<<Pente_obs[i]<<"   ";
       if (Statsup_Pente[i]<0) Statsup_Pente[i]=-1; 
       if (nb_repetitions>1) g<<Statsup_Pente[i]<<"   ";
       g<<endl;
     }                               

}



void melange_individus_dans_UGcoord(vector<int> v1)
{

  int j,k,individu,new_individu;
  random_shuffle(v1.begin(), v1.end()); 

  //reconstruction du tableau data a partir du tableau data_sauve
  for (j=0;j<nombre_UG;j++)
    {
      for (k=0;k<UG[j][2];k++)
	{
	  individu=int(UGcoord[j][k+3]);
	  new_individu=v1[individu];
	  UGcoord[j][k+3]=new_individu;
	 
	}
    }


}
//*********************************************************
//Programme principal
//*********************************************************

int main(int argc, char **argv)
{
  
  ////TApplication theApp("App", &argc, argv);


int i,j;
//double dist;
//char ouinonind,outputunit, typemarker;
//Matrice_occurence.Init(0,MAX_ALLELE,0,MAX_ALLELE);

char inputunit,polcar;
double dist;
double z,temp;



cout<<"*****************************************************";
cout<<endl<<"Programme GENBAR"<<endl;
 cout<<endl<<"The program analyses AFLP data"<<endl;
 cout<<"*****************************************************"<<endl<<endl;

cout<<"Give the name of the data file : ";
cin >>fic_data;
cout<<"Give the number of individuals  : ";
cin>>nombre_individus;
cout<<"Give the number of loci : ";
cin>>nombre_locus;
cout<<endl;

//Initialisation des tableaux
data.Init(0,nombre_individus-1,0,2+nombre_locus);
Espacet.Init(0,100,0,100);//tableau des UGt indices utilise pour Voisin, au max grille de 100x100
UGt.Init(0,2500,0,nombre_individus+3); //2500 nbre prévisionnel d'UG max
//contient les coordonnes des UG, le nombre d'ind par UG et le numero des ind de l'UG
Ind.Init(0,nombre_individus-1,0,1000);
vector<int> v1(nombre_individus, 0);

extraction_data(); 
cherche_minmax(); //cherche les latitudes minimales et maximales (en km)


 cout<<"unit of the geographical location  of the sampled individuals : Polar (deg) or Cartesian (km) ? Enter P ou C :";  
 cin>>inputunit;
 if (inputunit=='C')
   {
cout<<"-----------------------------------"<<endl;
cout<<"Parameters defining Spatial Zones : "<<endl;
cout<<"-----------------------------------"<<endl;
cout<<"Your data are distributed in an area whith following characteristics : "<<endl;
cout<<"Longitudes (km) Minimale : "<<minX/1<<"  Maximale :"<<maxX/1<<endl;// /1 permet d'avoir décimales
cout<<"Latitudes  (km) Minimale : "<<minY/1<<"  Maximale :"<<maxY/1<<endl;
//calcul de la distance en km entre ns et eo
 cout<<" Distance Est-Ouest : "<<maxX-minX<<endl;
 cout<<" Distance Nord-Sud : "<<maxY-minY<<endl;
cout<<endl;
cout<<"Choose the distance (d, in kms) between two centers of the spatial zone : ";
cin>>dist;
dist=dist;
cout<<"Choose the value of the radius of the spatial zones (r, in kms): ";
cin>>rayon;rayon=rayon;
cout<<"Choose the minimal number of invividuals in each spatial zone (n, minimum advised of 20) : ";
cin>>effectif_min;
cout<<endl;
 polcar='c';
   }

 if (inputunit=='P')
   {
cout<<"Longitudes (deg) Minimale : "<<minX/1<<"  Maximale :"<<maxX/1<<endl;
cout<<"Latitudes  (deg) Minimale : "<<minY/1<<"  Maximale :"<<maxY/1<<endl; 
cout<<"For information : et the poles, latitude = 90, at the equator, latitude = 0"<<endl; 
//calcul de la distance en km entre ns et eo 
z=cos(M_PI*minY/180)*cos(M_PI*maxY/180); 
z=z+(sin(M_PI*minY/180)*sin(M_PI*maxY/180)); 
temp=6400*acos(z); 
cout<<" Distance maximale Nord-Sud  : "<<temp<<endl; 
z=cos(M_PI*maxY/180)*cos(M_PI*maxY/180)*cos(M_PI*(maxX-minX)/180); 
z=z+(sin(M_PI*maxY/180)*sin(M_PI*maxY/180)); 
temp=6400*acos(z);  
cout<<" Distance Est-Ouest : "<<temp<<" a la latitude "<<maxY/1<<"deg"<<endl; 
z=cos(M_PI*minY/180)*cos(M_PI*minY/180)*cos(M_PI*(maxX-minX)/180); 
z=z+(sin(M_PI*minY/180)*sin(M_PI*minY/180)); 
temp=6400*acos(z);  
cout<<" Distance Est-Ouest : "<<temp<<" a la latitude "<<minY/1<<"deg"<<endl; 
cout<<endl; cout<<"Choose the distance (d, in kms) between two centers of the spatial zone : "; 
cin>>dist; dist=dist; 
cout<<"Choose the value of the radius of the spatial zones (r, in kms): "; 
cin>>rayon;rayon=rayon; cout<<"Choose the minimal number of invividuals in each spatial zone (n, minimum advised of 20) : "; 
cin>>effectif_min; 
cout<<endl; 
   }

/**********************************************************************


debut de la grande boucle


 ***********************************************************************/ 

char significance;

if (inputunit=='C') unite_geographique_cart(rayon,dist,effectif_min);
  else unite_geographique_pol(rayon,dist,effectif_min);

cout<<"Do you want to assess the significance of slopes ? (y/n) ";
 cin>>significance;
 if (significance=='y')
 {
	cout<<"How many random permutations do you want ? (minimum advised of 1000)";
 	cin>>nb_repetitions;
 }
 else nb_repetitions=1;

for (i=0;i<nombre_individus;i++) v1[i]=i;
Statsup_Pente.Init(0,nombre_UG);
Pente_obs.Init(0,nombre_UG);
 cout<<endl<<"I am working ..... "<<endl;
for (nbr=0;nbr<nb_repetitions;nbr++)
{

Nbgenes.Init(0,nombre_UG,0,nombre_locus);
UG.Init(0,nombre_UG,0,3+nombre_individus+nombre_locus);

if (nbr>0) 
melange_individus_dans_UGcoord(v1);
if (nb_repetitions > 1) 
  {
    cout<<nbr+1<<"  "<<flush;
  }




 for (i=0;i<nombre_UG+1;i++)
   for (j=0;j<3+nombre_individus;j++)
     UG[i][j]=UGcoord[i][j];
proba.Init(0,nombre_UG-1,0,nombre_individus);
Pente.Init(0,nombre_individus+1,0,nombre_UG); //tableau des pentes pour chaque
//individu dans ch UG, les trois dernieres lignes contiennent la moyenne et la
//variance pour tous les individus dans une UG.

//affiche UG


creation_tableau_Ind();
calcul_frequence_bandesAFLP_par_UG();
frequence_alleles.Init(0,nombre_UG-1,0,nombre_locus);






//affiche_totaux();


for (i=0;i<nombre_individus;i++)
  {
  construction_matrice_freq_all(); 
  recalcul_frequences_bandes(i); //on recalcule les occurences all en enlevant l'ind
      //on calcule les freq
//cin>>attend;
  calcul_proba_genotype_par_ours(i); 
  }
genere_un_seul_fichier_proba();
calcul_la_pente();

int nug;
if (nbr==0) 
  for (nug=0;nug<nombre_UG;nug++)
    Pente_obs[nug]=Pente[nombre_individus][nug];

for (nug=0;nug<nombre_UG;nug++)
{
  
   if (Pente[nombre_individus][nug]==0) {Statsup_Pente[nug]--;}
   else if (Pente[nombre_individus][nug]>Pente_obs[nug])
     Statsup_Pente[nug]++;
}
}
int nug;
for (nug=0;nug<nombre_UG;nug++)   {Statsup_Pente[nug]=Statsup_Pente[nug]/(nb_repetitions-1);}

if (inputunit=='C') genere_fichier_pente_cart();
    else genere_fichier_pente_pol();






//afficher frequence allele  apres avoir enleve l'individu
 int k;
  /*ofstream g("freq_allele_reco.txt");
  for (k=0;k<nombre_UG;k++)
	{
		g<<k<<",";
		for (j=0;j<nombre_locus;j++)
    			{
     			 g<<frequence_alleles[k][j]<<" , ";
			}
     		 g<<endl;
	}

*/
  //ofstream m("UG.txt");

  

return 0;
}

